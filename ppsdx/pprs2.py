__author__ = 'marco'

import argparse
import pickle
from multiprocessing.connection import Listener, Client
import os
import signal
import Queue
from threading import Thread
import sys
np = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
if np not in sys.path:
    sys.path.append(np)
import util.log
from xrs.server import server as Server
from route import Route
from Crypto.Cipher import AES
from participant_db import ParticipantDB
import subprocess
import math
from time import sleep

logger = util.log.getLogger('PPRS2')

NUMBER_OF_PARTICIPANTS=3

ABY_EXEC_PATH="/home/ubuntu/pp-ixp/aby/bin/ixp.exe"
#ABY_EXEC_PATH="/home/vagrant/aby/bin/ixp.exe"

AS_ROW_ENCODING_SIZE = 32 # bits
KEY_LENGTH = 16
KEY_ID_SIZE = 8 # bits

class PPRouteServer2:
    def __init__(self,single):
        logger.info("Initializing the Route Server 2.")

        # Init the Route Server
        self.server = None
        self.single = single

        # Initialize a XRS Server
        self.server = Server(logger, ("localhost", 6001), None)
        self.run = True

    def start(self):
        logger.info("Starting the Server to handle incoming messages from RS1")
        self.server.start()

        waiting = 0

        self.prefix2route_id_db = {}
        self.keys_db = {} # route_id 2 key
        self.prefix2nexthop2export_policy_db = {} # prefix 2 neighbor 2 export policy
        self.participant_db = ParticipantDB()
        self.number_of_participants = len(self.participant_db.bgp_speaker_2_id.keys())

        #sleep(5)
        # start the MPC process in background
        if self.single:
            args = (ABY_EXEC_PATH +' -r 1 -a localhost -o 1')
        else:
            args = (ABY_EXEC_PATH +' -r 1 -a localhost -o 1 -f 1')

        self.p = subprocess.Popen(args.split(" "),stdout=subprocess.PIPE,stdin=subprocess.PIPE, bufsize=8096)
        self.p.stdout.readline()

        while self.run:
            # get messages
            try:
                msg = self.server.receiver_queue.get(True, 1)
                msg = pickle.loads(msg)

                waiting = 0

                logger.debug("Got message from RS1. "+str(msg)+' '+str(type(msg)))

                if "stop" in msg:
                    logger.info("Shutting down.")
                    break

                self.process_message(msg)



            except Queue.Empty:
                if waiting == 0:
                    logger.debug("Waiting for BGP update...")
                    waiting = 1
                else:
                    waiting = (waiting % 30) + 1
                    if waiting == 30:
                        logger.debug("Waiting for BGP update...")

        myinput = "0\n"
        print >> self.p.stdin, myinput # write input
        self.p.stdin.flush() # not necessary in this case

    def stop(self):
        logger.info("Stopping.")
        self.run = False

    def process_message(self, msg):
        route_id=msg["route-id"]
        key=msg["key"]
        encrypted_export_policy=msg["encrypted_policy"]
        if self.single:
            ranked_route_ids=msg["ranking"]
        prefix=msg["prefix"]
        neighbor=msg["neighbor"]
	
	#print "route-id: " + str(route_id)
	#print " prefix: " + str(prefix)
        #print " ranking: " + str(ranked_route_ids)

        if self.single:
            #-1. store route-id per-prefix
            if prefix not in self.prefix2route_id_db:
                self.prefix2route_id_db[prefix]={}
            self.prefix2route_id_db[prefix][neighbor]=route_id

        if self.single:
            # 1. store encrypted export policy in EP-db
            if prefix not in self.prefix2nexthop2export_policy_db:
                self.prefix2nexthop2export_policy_db[prefix]={}
                for bgp_speaker in self.participant_db.bgp_speaker_2_id.keys():
                    self.prefix2nexthop2export_policy_db[prefix][bgp_speaker] = {}
            for bgp_speaker in self.participant_db.bgp_speaker_2_id.keys():
                self.prefix2nexthop2export_policy_db[prefix][bgp_speaker][route_id]=encrypted_export_policy[self.participant_db.bgp_speaker_2_id[bgp_speaker]]

        # 2. invoke MPC
        if self.single:
            number_of_routes = len(ranked_route_ids)+1
        else:
            number_of_routes = 2
        logger.debug("number of routes: " + str(number_of_routes))
        logger.debug("number of routes in export matrix: " + str(AS_ROW_ENCODING_SIZE))
        export_rows_string=""
        if self.single:
            for bgp_speaker in self.participant_db.bgp_speaker_2_id.keys():
                sum=0
                row = map(lambda x : self.prefix2nexthop2export_policy_db[prefix][bgp_speaker][x], reversed(ranked_route_ids))
                logger.debug("export row: " + str(row))
                for i in range(0,number_of_routes-1):
                    sum = sum << 1
                    if row[i]==True:
                        sum+=1
                export_row_string="{0:#0{1}x}".format(sum,AS_ROW_ENCODING_SIZE/4+2)[2:]
                export_rows_string+=export_row_string
        else:
            for i in range(0,len(self.participant_db.bgp_speaker_2_id.keys())):
                sum =0
                if encrypted_export_policy[i]:
                    sum+=1
                export_row_string="{0:#0{1}x}".format(sum,AS_ROW_ENCODING_SIZE/4+2)[2:]
                export_rows_string+=export_row_string
        logger.debug("export_rows_string: " + export_rows_string)

        # create the keys input
        #print self.prefix2route_id_db[prefix]
        keys_str = ""
        if self.single:
            for route_id in  self.prefix2route_id_db[prefix].values():
                logger.debug("2 -- Key for " + str(route_id) + " added to keys-db + value:")
                keys_str+="{0:#0{1}x}".format(0,2*KEY_LENGTH+KEY_ID_SIZE/4+2)[2:]
        else:
            keys_str+=key.encode('hex')
            keys_str+="{0:#0{1}x}".format(0,KEY_ID_SIZE/4+2)[2:]
         #add dummy key
        keys_str+="{0:#0{1}x}".format(0,2*KEY_LENGTH+KEY_ID_SIZE/4+2)[2:]

        # invoking the MPC
        myinput = str(self.number_of_participants) +"\n" +str(number_of_routes) +"\n" +export_rows_string + "\n" + keys_str 
        #args = (ABY_EXEC_PATH +' -r 0 -a localhost -b '+str(number_of_routes)+' -m '+str(self.number_of_participants)+' -o 1')
        logger.debug("input-to-mpc: " + myinput)
        #p = subprocess.Popen(args.split(" "),stdout=subprocess.PIPE,stdin=subprocess.PIPE)
        print >> self.p.stdin, myinput # write input
        #print "writing MPC: ", myinput
        self.p.stdin.flush() # not necessary in this case
        #str_stdout = self.p.communicate(input=myinput)[0]
        self.p.stdout.readline()
        #print "reading MPC"
        for i in range(0,self.number_of_participants):
            logger.debug(self.p.stdout.readline().rstrip())
        #logger.debug("output of the MPC:" + str_stdout.decode())



def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--single', action='store_true')
    parser.add_argument('--all', action='store_true')
    args = parser.parse_args()

    if args.single==False and args.all==False :
        args.single=True
    if args.single==True and args.all==True :
        args.all=False


    # locate config file
    # base_path = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)),"..","examples",args.dir,"config"))
    # config_file = os.path.join(base_path, "sdx_global.cfg")

    # start route server
    # sdx_rs = route_server(config_file)
    pprs = PPRouteServer2(args.single)
    rs_thread = Thread(target=pprs.start)
    rs_thread.daemon = True
    rs_thread.start()

    while rs_thread.is_alive():
        try:
            rs_thread.join(1)
        except KeyboardInterrupt:
            pprs.stop()


if __name__ == '__main__':
    main()
