
import sys
import os
np = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
if np not in sys.path:
    sys.path.append(np)
import argparse
import pickle
import util.log
import multiprocessing as mp
from Queue import Empty
import subprocess
from participant_db import ParticipantDB
from util.statistics_collector_2 import StatisticsCollector
from member_preferences import  MemberPreferences
from xrs.server import server as Server
from multiprocessing.connection import Listener, Client
import port_config
import threading
from util.nbstreamreader import NonBlockingStreamReader
import time

logger = util.log.getLogger('worker-all-rs1')


ABY_EXEC_PATH="../aby/bin/ixp.exe"
#ABY_EXEC_PATH="/home/vagrant/aby/bin/ixp.exe"

DUMMY_KEY="00000000000000000000000000000000ff"

KEY_LENGTH = 16

AS_ROW_ENCODING_SIZE = 32 # bits

KEY_ID_SIZE = 8 # bits

PARALLELIZATION_FACTOR = 1

MOCK = False

RS1=0

FIRST_PORT=7760


class AllWorker1():

    def __init__(self,id):
        # get port
        self.id = id
        self.smpc_port = port_config.ports_assignment["worker_port"]+ self.id
        self.member_receiver_port = port_config.ports_assignment["rs1_receive_bgp_messages"]+id

        logger.debug("launching ixp.exe on port " + str(self.smpc_port))

        self.rs1_to_rs2_client = Client((port_config.process_assignement["rs2"], port_config.ports_assignment["rs1_rs2"]+id), authkey=None)
        logger.debug("connected to rs2")
        #self.server_send_mpc_output = Server(logger, endpoint=(port_config.process_assignement["rs1"], port_config.ports_assignment["rs1_send_mpc_output"]+id),authkey=None)
        self.server_receive_bgp_messages = Server(logger, endpoint=(port_config.process_assignement["rs1"], self.member_receiver_port),authkey=None)
        #args = (ABY_EXEC_PATH +' -r 0 -a localhost -o 1 -f 2 -p ' + str(self.smpc_port))
        logger.info("taskset -c " + str(self.smpc_port-FIRST_PORT) + " " +ABY_EXEC_PATH +' -r 0 -a 0.0.0.0 -o 1 -f 3 -p ' + str(self.smpc_port))
        args = ( ABY_EXEC_PATH +' -r 0 -a 0.0.0.0 -o 1 -f 3 -p ' + str(self.smpc_port))
        #args = ("taskset -c " + str(self.smpc_port-FIRST_PORT) + " " + ABY_EXEC_PATH +' -r 0 -a 0.0.0.0 -o 1 -f 3 -p ' + str(self.smpc_port))
        self.p = subprocess.Popen(args.split(" "),stdout=subprocess.PIPE,stdin=subprocess.PIPE, bufsize=8096)
        #self.p.stdout.readline()
        self.reader= NonBlockingStreamReader(self.p.stdout)
        line = self.reader.readline(10)
        logger.debug("first line read: " + line.rstrip())

        logger.debug("process launched")
        self.participant_db = ParticipantDB()
        self.number_of_participants = len(self.participant_db.bgp_speaker_2_id)
        self.mock_counter=0
        self.statistics = StatisticsCollector()
        self.member_preference = MemberPreferences()
        self.rib_state=False


        self.prefix_2_bgp_next_hop_2_route = {}

    # process a BGP route
    def process_update(self):

        logger.info("Starting the Worker Server to handle incoming BGP Updates from ExaBGP. Listening on port" + str(self.member_receiver_port))
        self.server_receive_bgp_messages.start()
        logger.info("Connected to member via port " + str(self.member_receiver_port))
        #self.server_send_mpc_output.start()
        #logger.info("RS1 connected to Host Receiver Mock ")
        max_kill=100

        print str(self.id) + " is ready"
        while True:

            try:
                logger.info("waiting for handler message")
                msg = self.server_receive_bgp_messages.receiver_queue.get(True, 1)
                msg = pickle.loads(msg)
            except Empty:
                continue

            if "stop" in msg:
                logger.info("stop received")
                print "stop received" + str(self.id)
                self.rs1_to_rs2_client.send(pickle.dumps(msg))
                break
            elif "start-rib" in msg:
                logger.info("start-rib received")
                self.rib_state=True
                continue
            elif "finish-rib" in msg:
                logger.info("finish-rib received")
                print "finish-rib received"
                self.rib_state=False
                continue


            announcement_id = msg["announcement_id"]
            self.statistics.received_bgp_update(announcement_id)

            self.rs1_to_rs2_client.send(pickle.dumps({"type" : "to-rs2", "announcement_id" : announcement_id, "worker_port" : self.smpc_port}))


            if msg["prefix"] not in self.prefix_2_bgp_next_hop_2_route.keys():
                self.prefix_2_bgp_next_hop_2_route[msg["prefix"]]={}
            self.prefix_2_bgp_next_hop_2_route[msg["prefix"]][msg["bgp_next_hop"]] = msg
            logger.info("processing route " + str(msg["announcement_id"]))
            msg={"bgp_next_hop" : msg["bgp_next_hop"], "prefix" : msg["prefix"], "announcement_id" : msg["announcement_id"], "messages" : self.prefix_2_bgp_next_hop_2_route[msg["prefix"]]}

            logger.info("msg: " + str(msg))
            list_of_msg_for_prefix=msg["messages"] #remove the route_id used to prioritize the announcements in the queue

            if msg["announcement_id"] % 100 ==0:
                print "worker" + str(msg["announcement_id"])

            if self.rib_state:
                self.statistics.remove_statistic(announcement_id)
                continue

            prefix = msg["prefix"]

            encrypted_route = msg["messages"][msg["bgp_next_hop"]]["encrypted_route"]

            # RUNNING THE MPC
            number_of_routes = len(list_of_msg_for_prefix)+1

            #self.statistics.pre_export(announcement_id)
            export_rows_string=""
            i=0
            for bgp_speaker in self.participant_db.bgp_speaker_2_id.keys():
                sum=0
                for next_hop in list_of_msg_for_prefix.keys():
                    sum = sum << 1
                    if list_of_msg_for_prefix[next_hop]["encrypted_exp_policies_rs1"][i]==True:
                        sum+=1
                sum = sum << 1
                i+=1
                #export_row_string="{0:#0{1}x}".format(sum,AS_ROW_ENCODING_SIZE/4+2)[2:]
                export_row_string='{num:0{width}x}'.format(num=sum, width=AS_ROW_ENCODING_SIZE/4)
                export_rows_string+=export_row_string
            logger.debug("export_rows_string: " + export_rows_string)

            #self.statistics.pre_keys(announcement_id)
            #add dummy key
            keys_str = DUMMY_KEY

            i=1
            list_of_route_ids = [] # helpful for the member-receiving-mock
            for next_hop in list_of_msg_for_prefix.keys():
                #logger.info("list_of_msg: " + str(list_of_msg_for_prefix))
                #logger.info("msg: " + str(next_hop))
                #logger.info("msg[\"announcement_id\"]: " + str(list_of_msg_for_prefix[next_hop]["announcement_id"]))
                #logger.info(" list_of_route_ids: " + str(list_of_route_ids))
                list_of_route_ids.append(list_of_msg_for_prefix[next_hop]["announcement_id"])
                #logger.info("new_key: " + str(list_of_msg_for_prefix[next_hop]["key"]))
                keys_str+=list_of_msg_for_prefix[next_hop]["key"]
                #logger.info("key_str: " + str(keys_str))
                keys_str+='{num:0{width}x}'.format(num=i, width=KEY_ID_SIZE/4)
                #logger.info("key_str + id: " + str(keys_str))
                i+=1

            if len(list_of_route_ids)>1:
                local_prefs=""
                #create local prefs file
                #logger.info("member_preference: " + str(self.member_preference.member_2_member_2_local_pref))
                #self.statistics.pre_prefs(announcement_id)
                for bgp_speaker in self.participant_db.bgp_speaker_2_id.keys():
                    local_prefs+="00"
                    pre_look_up = self.member_preference.member_2_member_2_local_pref[self.participant_db.bgp_speakers_2_asnumber[bgp_speaker]]
                    for next_hop in list_of_msg_for_prefix.keys():
                        if next_hop == bgp_speaker:
                            local_pref="00"
                        else:
                            bgp_next_hop  = list_of_msg_for_prefix[next_hop]["bgp_next_hop"]
                            #print "8359" in self.member_preference.member_2_member_2_local_pref.keys()
                            local_pref = pre_look_up[self.participant_db.bgp_speakers_2_asnumber[bgp_next_hop]][RS1]
                        local_prefs+=local_pref
                myinput = "2"+"\n"+str(self.number_of_participants) +"\n" +str(number_of_routes) + "\n" + export_rows_string + "\n" + keys_str +  "\n" + local_prefs + "\n"+ "0"
            else:
                myinput = "1"+"\n"+str(self.number_of_participants) +"\n" +str(number_of_routes) + "\n" + export_rows_string + "\n" + keys_str +  "\n"+ "0"

            # invoking the MPC
            self.statistics.record_start_smpc(announcement_id)
            #print "id: " + str(announcement_id)
            logger.info("input-to-mpc: " + myinput)
            #self.statistics.record_start_smpc(announcement_id)
            print >> self.p.stdin, myinput # write input
            self.p.stdin.flush() # not necessary in this case
            self.statistics.record_start_smpc_input_sent(announcement_id)
            logger.debug("reading line")
            #logger.debug(self.p.stdout.readline())

            line = self.reader.readline(1)
            if line == None:
                    print "killing ixp.exe on port " + str(self.smpc_port)
                    self.p.terminate()
                    self.statistics.remove_statistic(announcement_id)
                    if max_kill >= 0:
                            time.sleep(1) # this should hopefully guarantee that also the other worker is killed
                            args = ("taskset -c " + str(self.smpc_port-FIRST_PORT) + " " + ABY_EXEC_PATH +' -r 0 -a 0.0.0.0 -o 1 -f 3 -p ' + str(self.smpc_port))
                            self.p = subprocess.Popen(args.split(" "),stdout=subprocess.PIPE,stdin=subprocess.PIPE, bufsize=8096)
                            self.reader = NonBlockingStreamReader(self.p.stdout)
                            self.reader.readline(10)
                            #p.stdout.readline()
                            print "re-launching ixp.exe port " + str(self.smpc_port)
                            if_first=True
                            max_kill-=1
                            continue
                    else:
                            break
            else:
                logger.debug("line: " + line.rstrip())


            self.statistics.record_end_of_smpc_processing(announcement_id)
            str_host=""
            if len(list_of_route_ids)>1:
                line = self.reader.readline(1)
                logger.debug("line: " + line.rstrip())
            for i in range(0,self.number_of_participants):
                #line=self.p.stdout.readline().rstrip()
                line=self.reader.readline(1).rstrip()
                logger.info(line)
                str_host+=line
            #for i in range(0,self.number_of_participants):
            #    self.p.stdout.readline()

            #logger.info("Sending route to Host. Key: " +str_host)
            self.statistics.record_end_of_bgp_update_processing(announcement_id)
            ###self.server_send_mpc_output.sender_queue.put(pickle.dumps({"type" : "to-hosts",  "announcement_id" : announcement_id, "encrypted_route" : encrypted_route, "key" : str_host, "prefix" : prefix, "list_of_route_ids" : list_of_route_ids}))


            #self.server_send_mpc_output.sender_queue.put(pickle.dumps({"encrypted_route" : encrypted_route, "key" : str_host}))

        #close the process

        myinput = "4\n"
        print >> self.p.stdin, myinput # write input
        self.p.stdin.flush() # not necessary in this case

        self.rs1_to_rs2_client.close()
        self.server_receive_bgp_messages.conn.close()

        f=open('statistics-prio-rs1-worker-'+str(self.smpc_port)+'.txt','w')
        f.write("route_id start-processing-time start-smpc-time smpc-time-no-output smpc-time-no-output-input-sent total-time smpc-time\n")
        for bgp_update_id in self.statistics.observations.keys():
            start_processing_time= self.statistics.observations[bgp_update_id]["start-processing-time"]
            start_smpc_time= self.statistics.observations[bgp_update_id]["start-smpc-time"]
            start_smpc_time_input_sent= self.statistics.observations[bgp_update_id]["start-smpc-time-input-sent"]
            smpc_time_no_output_reading= self.statistics.observations[bgp_update_id]["end-smpc-time"] - start_smpc_time
            smpc_time_no_output_reading_input_sent = self.statistics.observations[bgp_update_id]["end-smpc-time"] - start_smpc_time_input_sent
            total_time = self.statistics.observations[bgp_update_id]["end-processing-time"] - self.statistics.observations[bgp_update_id]["start-processing-time"]
            smpc_time = self.statistics.observations[bgp_update_id]["end-processing-time"] - self.statistics.observations[bgp_update_id]["start-smpc-time"]
            f.write(str(bgp_update_id) + " " + str("{0:.9f}".format(start_processing_time)) + " " + str("{0:.9f}".format(start_smpc_time)) + " " + str("{0:.9f}".format(smpc_time_no_output_reading))  + " " + str("{0:.9f}".format(smpc_time_no_output_reading_input_sent)) + " " + str("{0:.9f}".format(total_time)) +" " + str("{0:.9f}".format(smpc_time)) + "\n")
        f.close()

def prio_worker1_main(id):
    worker = AllWorker1(int(id))
    worker.process_update()

def main():
    parser = argparse.ArgumentParser()
        # locate config file
    # base_path = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)),"..","examples",args.dir,"config"))
    # config_file = os.path.join(base_path, "sdx_global.cfg")

    parser.add_argument("-i","--id", help="port number", type=int, default=0)
    parser.add_argument("-p","--processes", help="number of processes", type=int, default=1)
    args = parser.parse_args()
    processes=int(args.processes)
    procs = [mp.Process(target=prio_worker1_main, args=(x, )) for x in range(0,processes)]

    # Run processes
    for p in procs:
        p.start()

    # start route server
    # sdx_rs = route_server(config_file)
    #worker = AllWorker1(int(args.id))
    #worker.process_update()

    for p in procs:
        p.join()

    print "waiting before dying"
    logger.info("waiting before dying")

    for thread in threading.enumerate():
        print thread.name

if __name__ == '__main__':
    main()
