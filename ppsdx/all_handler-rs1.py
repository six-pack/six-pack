'''
receives BGP messages and assign them to the set of SMPC workers
'''


import argparse
import json
from multiprocessing.connection import Listener, Client
import os
import signal
import Queue
from threading import Thread
import sys
np = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
if np not in sys.path:
    sys.path.append(np)
import util.log
from xrs.server import server as Server
from route import Route
import route as route_static
import random
import os
import pickle
#from util.crypto_util import AESCipher
from functools import cmp_to_key
import math
from participant_db import ParticipantDB
import subprocess
from time import sleep
import time
from util.statistics_collector_2 import StatisticsCollector
import multiprocessing as mp
from multiprocessing import Process, Manager
import all_worker_rs1
from Queue import Empty
import threading
import port_config

logger = util.log.getLogger('all-handler-rs1')

ABY_EXEC_PATH="../aby/bin/ixp.exe"
#ABY_EXEC_PATH="/home/vagrant/aby/bin/ixp.exe"

DUMMY_KEY="00000000000000000000000000000000ff"

KEY_LENGTH = 16

AS_ROW_ENCODING_SIZE = 32 # bits

KEY_ID_SIZE = 8 # bits

class AllHandlerRs1:

    def __init__(self, number_of_processes):
        logger.info("Initializing the All Handler for RS1.")

        self.number_of_processes = number_of_processes
        

        # Initialize a XRS Server
        self.server_receive_bgp_messages = Server(logger, endpoint=(port_config.process_assignement["rs1"], port_config.ports_assignment["rs1_receive_bgp_messages"]),authkey=None)
        logger.info("Listening for BGP sender")
        self.server_send_mpc_output = Server(logger, endpoint=(port_config.process_assignement["rs1"], port_config.ports_assignment["rs1_send_mpc_output"]),authkey=None)
        logger.info("Listening for receiver mock")
        self.rs1_to_rs2_client = Client((port_config.process_assignement["rs2"], port_config.ports_assignment["rs1_rs2"]), authkey=None)
        logger.info("Connected to RS2.")
        self.run = True
        self.route_id_counter=0

        self.statistics_handler = StatisticsCollector()

        self.update_queue = mp.Manager().Queue()
        self.update_queue = mp.Queue()

        self.participant_db = ParticipantDB()
        self.number_of_participants = len(self.participant_db.bgp_speaker_2_id.keys())

        # create workers
        self.manager = Manager()
        self.handler_to_worker_queue = self.manager.Queue()
        self.worker_to_handler_queue = self.manager.Queue()
        self.worker_ids_queue = self.manager.Queue()
        map(self.worker_ids_queue.put,range(port_config.ports_assignment["worker_port"],port_config.ports_assignment["worker_port"]+self.number_of_processes))

        self.workers_pool = mp.Pool(self.number_of_processes, all_worker_rs1.all_worker_main,(self.handler_to_worker_queue,self.worker_to_handler_queue,self.worker_ids_queue,))

    def start(self):
        self.receive_bgp_routes_th = Thread(target=self.receive_bgp_routes)
        self.receive_bgp_routes_th.setName("self.receive_bgp_routes_th")
        self.receive_bgp_routes_th.start()

        self.receive_from_workers()

    def receive_from_workers(self):
        waiting =0
        stop_counter=self.number_of_processes
        while True:
            try:
                msg = self.worker_to_handler_queue.get(True, 1)

                logger.info("received message from worker")
                #logger.debug("received message from worker: " + str(msg))

                if "stop" in msg:
                    logger.info("received STOP message from worker")
                    stop_counter-=1
                    self.rs1_to_rs2_client.send(pickle.dumps(msg))
                    if stop_counter == 0:
                        self.server_send_mpc_output.sender_queue.put(pickle.dumps(msg))
                        break
                    continue

                if msg["type"] == "to-rs2" or msg["type"] == "to-rs2-init":
                    logger.info("received TO-RS2 message from worker, type: " + str(msg["type"]) + " msg: " + str(msg) )
                    self.rs1_to_rs2_client.send(pickle.dumps(msg))

                if msg["type"] == "to-hosts":
                    logger.info("received TO-HOSTS message from worker")
                    self.server_send_mpc_output.sender_queue.put(pickle.dumps(msg))


            except Empty:
                if waiting == 0:
                    logger.info("Waiting for BGP update...")
                    waiting = 1
                else:
                    waiting = (waiting % 30) + 1
                    if waiting == 30:
                        logger.info("Waiting for BGP update...")
        self.rs1_to_rs2_client.close()
        logger.info("shutting down receive from workers")



    def receive_bgp_routes(self):
        logger.info("Starting the Server to handle incoming BGP Updates from ExaBGP. Listening on port 6000")
        self.server_receive_bgp_messages.start()
        logger.info("Connected to ExaBGP via port 6000")
        self.server_send_mpc_output.start()
        logger.info("RS1 connected to Host Receiver Mock ")

        waiting=0
        while self.run:
            # get BGP messages from ExaBGP
            try:
                msg = self.server_receive_bgp_messages.receiver_queue.get(True, 1)
                msg = pickle.loads(msg)

                waiting = 0

                logger.info("Got bgp_route from ExaBGP. " + str(msg))
                #logger.debug("Got bgp_update from ExaBGP.")
                # Received BGP bgp_update advertisement from ExaBGP

                if "stop" in msg:
                    close_msg = {"stop" : 1}
                    for _ in range(0,self.number_of_processes):
                        logger.info("Send closing message " + str(close_msg))
                        self.handler_to_worker_queue.put(msg)
                    break
                else:
                    self.statistics_handler.route_inserted_into_queue(msg["route_id"])
                    self.handler_to_worker_queue.put(msg)


            except Queue.Empty:
                if waiting == 0:
                    logger.debug("Waiting for BGP update...")
                    waiting = 1
                else:
                    waiting = (waiting % 30) + 1
                    if waiting == 30:
                        logger.debug("Waiting for BGP update...")
        logger.debug("receive_routes_shut_down")

        '''if self.single:
            f=open('statistics-single.txt','w')
        else:
            f=open('statistics-all.txt','w')
        f.write("route_id pre_mpc mpc total\n")
        for bgp_update in self.statistics.observations.keys():
            for route_id in self.statistics.observations[bgp_update]["routes"].keys():
                total_pre_mpc_time = self.statistics.observations[bgp_update]["routes"][route_id]["total-time-python-no-mpc"]
                total_mpt_time = self.statistics.observations[bgp_update]["routes"][route_id]["total-mpc-time"]
                total_time = self.statistics.observations[bgp_update]["routes"][route_id]["total-time-after-mpc"]
                f.write(str(route_id) + " " + str(total_pre_mpc_time) + " " + str(total_mpt_time) + " " + str(total_time) + "\n")

        # saving statistics
        print str(self.statistics)'''

        f=open('statistics-all-rs1-handler.txt','w')
        f.write("route_id route-to-worker-time\n")
        for bgp_update_id in self.statistics_handler.observations.keys():
            route_to_worker_time= self.statistics_handler.observations[bgp_update_id]["route-to-worker-time"]
            f.write(str(bgp_update_id) + " " + str("{0:.15f}".format(route_to_worker_time)) + "\n")
        f.close()



    def stop(self):
        while True:
            try:
                self.receive_bgp_routes_th.join(1)
                logger.debug("waiting for join receive_bgp_routes")
            except KeyboardInterrupt:
                self.run=False
        logger.info("Stopping.")
        self.run = False

def main():
    parser = argparse.ArgumentParser()
        # locate config file
    # base_path = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)),"..","examples",args.dir,"config"))
    # config_file = os.path.join(base_path, "sdx_global.cfg")

    parser.add_argument("-p","--processes", help="number of parallel SMPC processes", type=int, default=1)
    args = parser.parse_args()

    # start route server
    # sdx_rs = route_server(config_file)
    pprs = AllHandlerRs1(args.processes)
    rs_thread = Thread(target=pprs.start)
    rs_thread.setName("AllHandler1Thread")
    rs_thread.daemon = True
    rs_thread.start()

    while rs_thread.is_alive():
        try:
            rs_thread.join(1)
            #logger.debug("waiting for join pprs")
            #logger.debug("join cycle")
        except KeyboardInterrupt:
            pprs.stop()
    logger.debug("waiting before dying")

    for thread in threading.enumerate():
        print thread.name


if __name__ == '__main__':
    main()
