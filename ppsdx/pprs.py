__author__ = 'marco'

import argparse
import json
from multiprocessing.connection import Listener, Client
import os
import signal
import Queue
from threading import Thread
import sys
np = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
if np not in sys.path:
    sys.path.append(np)
import util.log
from xrs.server import server as Server
from route import Route
import route as route_static
import random
import os
import pickle
from util.crypto_util import AESCipher
from functools import cmp_to_key
import math
from participant_db import ParticipantDB
import subprocess
from time import sleep
import time
from util.statistics_collector import StatisticsCollector

logger = util.log.getLogger('PPRS')

KEY_LENGTH = 16

ROUTE_SERVER_2_IP_ADDRESS = "localhost"
ROUTE_SERVER_2_PORT = 6001
ROUTE_SERVE_2_AUTH_KEY = "rs2"

#ABY_EXEC_PATH="/home/ubuntu/pp-ixp/aby/bin/ixp.exe"
#ABY_EXEC_PATH="/home/vagrant/aby/bin/ixp.exe"
ABY_EXEC_PATH="/home/ubuntu/marco-chiesa/sixpack/ppsdx/aby/"


AS_ROW_ENCODING_SIZE = 32 # bits
KEY_LENGTH = 16 #bytes
KEY_ID_SIZE = 8 # bits


class PPRouteServer:
    def __init__(self,single):
        logger.info("Initializing the Route Server.")

        # Init the Route Server
        self.server = None
        self.rs2 = None
        #self.ah_socket = None
        #self.ah_socket = ("localhost", 6666)
        self.single=single

        # Initialize a XRS Server
        self.server = Server(logger)
        self.run = True

        self.route_id_counter =0
        self.route_db = {} # route_id 2 route
        self.encrypted_route_db = {} # route_id 2 route
        self.prefix2nexthop2route_id_db = {} # prefix 2 next_hop 2 route_id
        self.prefix2nexthop2export_policy_db = {} # prefix 2 neighbor 2 export policy
        self.keys_db = {} # route_id 2 key
        self.participant_db = ParticipantDB()
        self.number_of_participants = len(self.participant_db.bgp_speaker_2_id.keys())
        self.statistics = StatisticsCollector()

        """
        Start the announcement Listener which will receive announcements
        from participants's controller and put them in XRS's sender queue.
        """
        #self.set_announcement_handler()

    def start(self):
        logger.info("Starting the Server to handle incoming BGP Updates from ExaBGP. Listening on port 6000")
        self.server.start()
        self.rs2 = Client(("localhost", 6001), authkey=None)
        self.host = Client(("localhost", 6005), authkey=None)
        logger.info("RS1 connected to RS2")
        logger.info("RS1 connected to Host")

        # start the MPC process in background
        if self.single:
            args = (ABY_EXEC_PATH +' -r 0 -a localhost -o 1')
        else:
            args = (ABY_EXEC_PATH +' -r 0 -a localhost -o 1 -f 1')

        self.p = subprocess.Popen(args.split(" "),stdout=subprocess.PIPE,stdin=subprocess.PIPE, bufsize=8096)
        self.p.stdout.readline()
        waiting = 0

        bgp_update_id=0
        while self.run:
            # get BGP messages from ExaBGP via stdin
            try:
                bgp_update = self.server.receiver_queue.get(True, 1)
                bgp_update = json.loads(bgp_update)

                waiting = 0

                logger.debug("Got bgp_update from ExaBGP. "+str(bgp_update)+' '+str(type(bgp_update)))
                #logger.debug("Got bgp_update from ExaBGP.")
                # Received BGP bgp_update advertisement from ExaBGP

                if bgp_update <10 :
                    continue

                if "stop" in bgp_update:
                    close_msg = {"stop" : 1}
                    logger.info("Shutting down.")
                    self.send_update(close_msg)
                    logger.info("Shutting down2.")
                    self.host.send(pickle.dumps(close_msg))
                    break
                else:
                    if "announce" in bgp_update["neighbor"]["message"]["update"]:
                        self.statistics.received_bgp_update(bgp_update_id)
                        self.process_announce_bgp_update(bgp_update)
                        self.statistics.record_end_of_bgp_update_processing(bgp_update_id)
                        pass
                    if "withdraw" in bgp_update["neighbor"]["message"]["update"]:
                        self.process_withdraw_bgp_update(bgp_update)
                        pass



            except Queue.Empty:
                if waiting == 0:
                    logger.debug("Waiting for BGP update...")
                    waiting = 1
                else:
                    waiting = (waiting % 30) + 1
                    if waiting == 30:
                        logger.debug("Waiting for BGP update...")

            bgp_update_id+=1

        myinput = "0\n"
        print >> self.p.stdin, myinput # write input
        self.p.stdin.flush() # not necessary in this case

        if self.single:
            f=open('statistics-single.txt','w')
        else:
            f=open('statistics-all.txt','w')
        f.write("route_id pre_mpc mpc total\n")
        for bgp_update in self.statistics.observations.keys():
            for route_id in self.statistics.observations[bgp_update]["routes"].keys():
                total_pre_mpc_time = self.statistics.observations[bgp_update]["routes"][route_id]["total-time-python-no-mpc"]
                total_mpt_time = self.statistics.observations[bgp_update]["routes"][route_id]["total-mpc-time"]
                total_time = self.statistics.observations[bgp_update]["routes"][route_id]["total-time-after-mpc"]
                f.write(str(route_id) + " " + str(total_pre_mpc_time) + " " + str(total_mpt_time) + " " + str(total_time) + "\n")
        
        # saving statistics
        print str(self.statistics)

    def process_announce_bgp_update(self,bgp_update):
        bgp_update = bgp_update["neighbor"]
        # parse communities from the bgp update
        export_policies_communities = []
        non_export_policies_communities = []
        if("community" in bgp_update["message"]["update"]["announce"]):
            split_communities = bgp_update["message"]["update"]["announce"]["community"].split(" ")
            for community in split_communities:
                if community.split(":")[0] == "0" or community.split(":")[0] == "6695":
                    export_policies_communities.append((community.split(":")[0],community.split(":")[1]))
                else:
                    non_export_policies_communities.append((community.split(":")[0],community.split(":")[1]))

        # for each bgp_update in the announcement
        for prefix in bgp_update["message"]["update"]["announce"]["ipv4 unicast"]:
            initial_time = time.time()
            route = Route()
            route.id = self.route_id_counter
            self.statistics.processing_route(route.id)
            #logger.debug("execution time: "+ str(time.time() - initial_time) )
            self.route_id_counter+=1
            route.prefix=prefix
            route.neighbor = bgp_update["ip"]
            route.as_path =self.parse_as_path(bgp_update["message"]["update"]["attribute"]["as-path"])
            route.next_hop = bgp_update["message"]["update"]["announce"]["nexthop"]
            route.communities = non_export_policies_communities
            logger.info("Route received: " + str(route))

            if self.single:
                #logger.debug("execution time: " + str(time.time() - initial_time) )
                #print "execution time: " + str(time.time() - initial_time)
                # 0. store route in route-db
                self.route_db[route.id]=route
                logger.debug("Route-db before update: " + str(self.prefix2nexthop2route_id_db))
                if route.prefix not in self.prefix2nexthop2route_id_db:
                    self.prefix2nexthop2route_id_db[route.prefix]={}
                self.prefix2nexthop2route_id_db[route.prefix][route.neighbor]=route.id
                logger.debug("Stored route " + str(route.id) + " in route-db")
                logger.debug("Route-db updated: " + str(self.prefix2nexthop2route_id_db[route.prefix]))
                #print "self.prefix2nexthop2route_id_db[route.prefix]:" + str(self.prefix2nexthop2route_id_db[route.prefix])
                #logger.debug("execution time: " + str(time.time() - initial_time) )
                #print "execution time: " + str(time.time() - initial_time)

            # 1. generate key for the incoming route
            key = os.urandom(KEY_LENGTH)
            logger.debug("Key for Route " + str(route.id) + " generated")

            if self.single:
                #logger.debug("execution time: " + str(time.time() - initial_time) )
                #print "execution time: " + str(time.time() - initial_time)
                # 2. store key in the key-db
                self.keys_db[route.id] = key
                logger.debug("Key for " + str(route.id) + " added to keys-db")

            #logger.debug("execution time: " + str(time.time() - initial_time) )
            #print "execution time: " + str(time.time() - initial_time)
            # 3. encrypt the route
            self.cipher = AESCipher(key)
            encrypted_route = self.cipher.encrypt(pickle.dumps(route)) #encrypt serialized route object
            logger.debug("Encrypted route " + str(route.id))

            #logger.debug("execution time: " + str(time.time() - initial_time) )
            #print "execution time: " + str(time.time() - initial_time)
            # 4. store encrypted route in enc-route-db
            self.encrypted_route_db[route.id] = encrypted_route
            logger.debug("Stored encrypted route " + str(route.id) + " in enc-db")

            if self.single:
                #logger.debug("execution time: " + str(time.time() - initial_time) )
                #print "execution time: " + str(time.time() - initial_time)
                # 5. BGP route ranking
                routes = map(self.get_route,self.prefix2nexthop2route_id_db[route.prefix].values())
                #logger.debug("available routes: " + str(routes))
                ranked_routes = sorted(routes,key=cmp_to_key(route_static.compare_routes))
                ranked_route_ids = map(lambda x : x.id,ranked_routes)
                logger.debug("ranked routes: " + str(ranked_route_ids))


            #logger.debug("execution time: " + str(time.time() - initial_time) )
            #print "execution time: " + str(time.time() - initial_time)

            # These steps should not happen here but we'll do it here
            # for convenience
            # 0. generate export data structure. Each export policy is modeled
            # by a list of boolean values: True if export is allowed, False otherwise
            logger.debug("generating export_policy structure")
            is_export_all=True
            # check whether it is a blacklist (ie, is_export_all=True) or a white-list (ie, is_export_all=False)
            for community in export_policies_communities:
                if(community[0]=="0" and community[1]=="6695"):
                    is_export_all=False
            if is_export_all:
                export_policy = [True] * self.number_of_participants
            else:
                export_policy = [False] * self.number_of_participants
            logger.debug("export policy is export all? " + str(is_export_all))
            logger.debug("export policy communities:  " + str(export_policies_communities))
            sum =0
            # extract the ASes that are black- or white-listed
            for community in export_policies_communities:
                if(community[0]=="0" and community[1]=="6695") or (community[0]=="6695" and community[1]=="6695"):
                    continue
                else:
                    logger.debug("community: " + str(community))
                    if community[1] not in self.participant_db.asnumber_2_bgp_speakers.keys():
                        continue
                    for bgp_speaker in self.participant_db.asnumber_2_bgp_speakers[community[1]]:
                        if bgp_speaker not in self.participant_db.bgp_speaker_2_id:
                            continue
                        index = self.participant_db.bgp_speaker_2_id[bgp_speaker]
                        logger.debug("community for speaker " + str(bgp_speaker) + " with id: " + str(index) + " as-number:" + str(community[1]))
                        logger.debug("community[0]: " + str(community[0]) + " community[1]==0 : " + str(community[1]=="6695") )
                        if community[0]=="0" and community[1]!="6695":
                            export_policy[index] = False
                            logger.debug("export-policy[index]: " + str(export_policy[index]))
                        if community[0]=="6695" and community[1]!="6695":
                            export_policy[index] = True

            logger.debug("export policy is: " + str(export_policy))
            # 1. generate nonce
            logger.debug("generating nonce")
            #random_nonce =  map(lambda x : random.random() > 0.5,[0] * self.number_of_participants)
            random_nonce =  [True] * self.number_of_participants

            #logger.debug("execution time: " + str(time.time() - initial_time) )
            #print "execution time: " + str(time.time() - initial_time)
            # 2. store the nonce in the EP-db
            if self.single:
                if route.prefix not in self.prefix2nexthop2export_policy_db:
                    self.prefix2nexthop2export_policy_db[route.prefix]={}
                    for bgp_speaker in self.participant_db.bgp_speaker_2_id.keys():
                         self.prefix2nexthop2export_policy_db[route.prefix][bgp_speaker] = {}
                for bgp_speaker in self.participant_db.bgp_speaker_2_id.keys():
                    self.prefix2nexthop2export_policy_db[route.prefix][bgp_speaker][route.id]=random_nonce[self.participant_db.bgp_speaker_2_id[bgp_speaker]]

            #logger.debug("execution time: "+ str(time.time() - initial_time) )
            # 3. XOR nonce with export policy
            logger.debug("encrypting the export policy")
            encrypted_export_policy = map(lambda x,y : x ^ y, export_policy, random_nonce)

            #logger.debug("execution time: " + str(time.time() - initial_time) )
            #print "execution time: " + str(time.time() - initial_time)
            # 4. send the XORed export policy, ranking and the key to RS2
            logger.debug("sending encrypted policy, ranking, and key to RS2")
            logger.debug("sending key: " + str(len(key)))

            if self.single:
                self.send_update({"encrypted_policy" : encrypted_export_policy, "ranking" : ranked_route_ids,"key" : key,"route-id" : route.id, "prefix" : prefix, "neighbor" : route.neighbor})
            else:
                self.send_update({"encrypted_policy" : encrypted_export_policy, "key" : key,"route-id" : route.id, "prefix" : prefix, "neighbor" : route.neighbor})

            self.statistics.record_time_before_creating_input_for_invoking_mpc(route.id)
            # RUNNING THE MPC
            # create the export table for the prefix
            #logger.debug("execution time: " + str(time.time() - initial_time) )
            #print "execution time: " + str(time.time() - initial_time)
            if self.single:
                number_of_routes = len(ranked_route_ids)+1
            else:
                number_of_routes = 2
            logger.debug("number of routes: " + str(number_of_routes))
            logger.debug("number of routes in export matrix: " + str(AS_ROW_ENCODING_SIZE))
            export_rows_string=""
            if self.single:
                for bgp_speaker in self.participant_db.bgp_speaker_2_id.keys():
                    sum=0
                    row = map(lambda x : self.prefix2nexthop2export_policy_db[route.prefix][bgp_speaker][x], reversed(ranked_route_ids))
                    logger.debug("export row: " + str(row))
                    for i in range(0,number_of_routes-1):
                        sum = sum << 1
                        if row[i]==True:
                            sum+=1
                    export_row_string="{0:#0{1}x}".format(sum,AS_ROW_ENCODING_SIZE/4+2)[2:]
                    export_rows_string+=export_row_string
            else:
                for i in range(0,len(self.participant_db.bgp_speaker_2_id.keys())):
                    sum =0
                    if random_nonce[i]:
                        sum+=1
                    export_row_string="{0:#0{1}x}".format(sum,AS_ROW_ENCODING_SIZE/4+2)[2:]
                    export_rows_string+=export_row_string
            logger.debug("export_rows_string: " + export_rows_string)

            logger.debug("execution time: " + str(time.time() - initial_time) )
            #print "execution time: " + str(time.time() - initial_time)
            # create the keys input
            keys_str = ""

            if self.single:
                i=0
                for route_id in self.prefix2nexthop2route_id_db[route.prefix].values():
                    logger.debug("generate key for " + str(route_id))
                    keys_str+=self.keys_db[route_id].encode('hex')
                    keys_str+="{0:#0{1}x}".format(i,KEY_ID_SIZE/4+2)[2:]
                    i+=1
            else:
                keys_str+=key.encode('hex')
                keys_str+="{0:#0{1}x}".format(0,KEY_ID_SIZE/4+2)[2:]
            #add dummy key
            keys_str+="00000000000000000000000000000000ff"

            #sleep(1.5)
            # invoking the MPC
            #print "execution time: " + str(time.time() - initial_time)
            myinput = str(self.number_of_participants) +"\n" +str(number_of_routes) + "\n" + export_rows_string + "\n" + keys_str 
            #print >> self.p.stdin, myinput # write input
            #self.p.stdin.flush() # not necessary in this case
            #args = (ABY_EXEC_PATH +' -r 0 -a localhost -b '+str(number_of_routes)+' -m '+str(self.number_of_participants)+' -o 1')
            logger.debug("input-to-mpc: " + myinput)
            #p = subprocess.Popen(args.split(" "),stdout=subprocess.PIPE,stdin=subprocess.PIPE)
            initial_time_mpc = time.time()

            #print "execution time: " + str(time.time() - initial_time)
            self.statistics.record_time_before_invoking_mpc(route.id)
            print >> self.p.stdin, myinput # write input
            #sleep(5)
            self.p.stdin.flush() # not necessary in this case
            #str_stdout = self.p.communicate(input=myinput)[0]
            #print "execution time: " + str(time.time() - initial_time)
            #sleep(5)
            self.p.stdout.readline()
            #print "execution time!: " + str(time.time() - initial_time)
            #logger.debug("execution time: " + str(time.time() - initial_time) )
            str_host=""
            for i in range(0,self.number_of_participants):
                line=self.p.stdout.readline().rstrip()
                logger.debug(line)
                str_host+=line
            self.host.send(pickle.dumps((str_host,random_nonce)))
            #sleep(0.1)
            #print "take a rest. Waiting one second!: " + str(time.time() - initial_time)
            #logger.debug("output of the MPC:" + str_stdout.decode())
            self.statistics.record_time_after_mpc_execution(route.id)
            #sleep(1)
            final_time = time.time()
            execution_time=final_time-initial_time
            execution_time_mpc=final_time-initial_time_mpc
            #logger.debug("execution time: " + str(execution_time.seconds) + "ss " + str(execution_time.microseconds) + "us")
            #logger.debug("execution time mpc: " + str(execution_time_mpc.seconds) + "s " + str(execution_time_mpc.microseconds) + "us")
            logger.debug("execution time: " + str(execution_time) )
            logger.debug("execution time mpc: " + str(execution_time_mpc) )
            
            logger.info("New route processed in :" + str(execution_time) + " route-id: " + str(route.id) )
            #print str(logger.getEffectiveLevel()) + " " + str(logger.DEBUG) +" " + str(logger.INFO)
        #logger.debug("Updated route-db : " + str(self.route_db))

    def create_route_object(self,bgp_update,prefix,non_export_policies_communities,route_id):
            initial_time = time.time()
            route = Route()
            route.id = route_id
            self.statistics.processing_route(route.id)
            #logger.debug("execution time: "+ str(time.time() - initial_time) )
            route.prefix=prefix
            route.neighbor = bgp_update["ip"]
            route.as_path =self.parse_as_path(bgp_update["message"]["update"]["attribute"]["as-path"])
            route.next_hop = bgp_update["message"]["update"]["announce"]["nexthop"]
            route.communities = non_export_policies_communities
            logger.info("Route received: " + str(route))


    def process_withdraw_bgp_update(self,bgp_update):
        #TODO
        pass

    def get_route(self,x):
        return self.route_db[x]

    def parse_as_path(self,as_path_xbgp_format):
        as_path_split = as_path_xbgp_format.split(" ")
        return as_path_split[1:len(as_path_split)-1]

    def parse_config(self, config_file):
        # loading config file

        logger.debug("Begin parsing config...")

        config = json.load(open(config_file, 'r'))

        self.ah_socket = tuple(config["Route Server"]["AH_SOCKET"])

        logger.debug("Done parsing config")

    def set_announcement_handler(self):
        '''Start the listener socket for BGP Announcements'''

        logger.info("Starting the announcement handler...")
        self.listener_eh = Listener(self.ah_socket, authkey=None)
        ps_thread = Thread(target=self.start_ah)
        ps_thread.daemon = True
        ps_thread.start()

    def send_update(self, msg):
        self.rs2.send(pickle.dumps(msg))
        # self.rs2.recv()

    def start_ah(self):
        '''Announcement Handler '''

        logger.info("Announcement Handler started. Listening on port 6666")

        while True:
            conn_ah = self.listener_eh.accept()
            tmp = conn_ah.recv()

            logger.debug("Received an announcement.")

            announcement = json.loads(tmp)
            self.server.sender_queue.put(announcement)
            reply = "Announcement processed"
            conn_ah.send(reply)
            conn_ah.close()

    def stop(self):
        logger.info("Stopping.")
        self.run = False
        self.rs2.close()

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--single', action='store_true')
    parser.add_argument('--allp', action='store_true')
    args = parser.parse_args()


    if args.single==False and args.allp==False :
        args.single=True
    if args.single==True and args.allp==True :
        args.allp=False

    # locate config file
    # base_path = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)),"..","examples",args.dir,"config"))
    # config_file = os.path.join(base_path, "sdx_global.cfg")

    # start route server
    # sdx_rs = route_server(config_file)
    pprs = PPRouteServer(args.single)
    rs_thread = Thread(target=pprs.start)
    rs_thread.daemon = True
    rs_thread.start()

    while rs_thread.is_alive():
        try:
            rs_thread.join(1)
            #logger.debug("join cycle")
        except KeyboardInterrupt:
            pprs.stop()
    logger.debug("waiting before dying")

if __name__ == '__main__':
    main()
