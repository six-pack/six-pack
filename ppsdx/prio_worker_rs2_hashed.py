
import sys
import os
np = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
if np not in sys.path:
    sys.path.append(np)
import pickle
import util.log
import argparse
from Queue import Empty
import subprocess
from participant_db import ParticipantDB
from member_preferences import MemberPreferences
import port_config
from util.statistics_collector_2 import StatisticsCollector
from xrs.server import server as Server
import multiprocessing as mp
from threading import Thread
import threading
from util.nbstreamreader import NonBlockingStreamReader
import time

logger = util.log.getLogger('worker-all-rs2')


ABY_EXEC_PATH="../aby/bin/ixp.exe"
#ABY_EXEC_PATH="/home/vagrant/aby/bin/ixp.exe"

DUMMY_KEY="0000000000000000000000000000000000"

KEY_LENGTH = 16

AS_ROW_ENCODING_SIZE = 32 # bits

KEY_ID_SIZE = 8 # bits

PARALLELIZATION_FACTOR = 1

MOCK = False

RS2 = 1

FIRST_PORT=7760

class AllWorker2():

    def __init__(self,id):
        # get port
        self.id = id
        self.smpc_port = port_config.ports_assignment["worker_port"]+ self.id
        self.member_receiver_port = port_config.ports_assignment["rs2_receive_bgp_messages"]+id
        self.run=True

        logger.debug("launching ixp.exe on port " + str(self.smpc_port))

        self.server_receive_bgp_messages = Server(logger, endpoint=(port_config.process_assignement["rs2"],self.member_receiver_port))
        #self.server_send_mpc_output = Server(logger, endpoint=(port_config.process_assignement["rs2"], port_config.ports_assignment["rs2_send_mpc_output"]+self.id))
        self.server_rs1 = Server(logger, endpoint=(port_config.process_assignement["rs2"],  port_config.ports_assignment["rs1_rs2"]+id))

        #args = (ABY_EXEC_PATH +' -r 1 -a localhost -o 1 -f 2 -p ' + str(self.smpc_port))
        logger.info("taskset -c " + str(self.smpc_port-FIRST_PORT) + " " +ABY_EXEC_PATH +' -r 1 -o 1 -f 3 -a ' + port_config.process_assignement["rs1"] + ' -p ' + str(self.smpc_port))
        args = (ABY_EXEC_PATH +' -r 1 -o 1 -f 3 -a ' + port_config.process_assignement["rs1"] + ' -p ' + str(self.smpc_port))
        #args = ("taskset -c " + str(self.smpc_port-FIRST_PORT) + " " +ABY_EXEC_PATH +' -r 1 -o 1 -f 3 -a ' + port_config.process_assignement["rs1"] + ' -p ' + str(self.smpc_port))
        self.p = subprocess.Popen(args.split(" "),stdout=subprocess.PIPE,stdin=subprocess.PIPE, bufsize=8096)
        #self.p.stdout.readline()
        self.reader= NonBlockingStreamReader(self.p.stdout)
        line = self.reader.readline(10)
        logger.debug("first line read: " + line.rstrip())
        self.max_kill=100

        logger.debug("process launched")
        self.participant_db = ParticipantDB()
        self.number_of_participants = len(self.participant_db.bgp_speaker_2_id)
        self.mock_counter=0
        self.member_preference = MemberPreferences()
        self.statistics = StatisticsCollector()
        self.prefix_2_bgp_next_hop_2_route = {}

        self.id_2_msg = mp.Manager().dict()
        self.id_2_port = mp.Manager().dict()

        self.lock = mp.Manager().Lock()

        self.receive_mappings_from_rs1_th = Thread(target=self.receive_mappings_from_rs1)
        self.receive_mappings_from_rs1_th.setName("receive_mappings_from_rs1_th")
        self.receive_mappings_from_rs1_th.daemon = True
        self.receive_mappings_from_rs1_th.start()

        self.rib_state=False

    def receive_mappings_from_rs1(self):
        waiting=0
        logger.info("connecting to RS1")
        self.server_rs1.start()
        logger.info("connected to RS1 for receiving mapping messages")
        while self.run:
            # get BGP messages from ExaBGP
            try:
                msg = self.server_rs1.receiver_queue.get(True, 1)
                msg = pickle.loads(msg)

                waiting = 0

                logger.info("Got mapping message from RS1. " + str(msg))
                #logger.debug("Got bgp_update from ExaBGP.")
                # Received BGP bgp_update advertisement from ExaBGP

                if "stop" in msg:
                    logger.info("received stop message")
                    break
                elif "start-rib" in msg:
                    self.rib_state=True
                    continue
                elif "finish-rib" in msg:
                    self.rib_state=False
                    continue
                elif "announcement_id" in msg:
                    logger.info("received route_id message")
                    announcement_id = msg["announcement_id"]
                    self.lock.acquire()
                    self.id_2_port[announcement_id] = msg["worker_port"]
                    if announcement_id in self.id_2_msg:
                        logger.info("adding route " + str(announcement_id) + " into the worker queue")
                        #send message to the correct worker
                        if self.id_2_msg[announcement_id]["prefix"] not in self.prefix_2_bgp_next_hop_2_route.keys():
                            self.prefix_2_bgp_next_hop_2_route[self.id_2_msg[announcement_id]["prefix"]]={}
                        self.prefix_2_bgp_next_hop_2_route[self.id_2_msg[announcement_id]["prefix"]][self.id_2_msg[announcement_id]["bgp_next_hop"]] = self.id_2_msg[announcement_id]
                        msg={"announcement_id" : msg["announcement_id"], "messages" : self.prefix_2_bgp_next_hop_2_route[self.id_2_msg[announcement_id]["prefix"]]}

                        del self.id_2_port[announcement_id]
                        del self.id_2_msg[announcement_id]

                        if self.rib_state:
                            self.lock.release()
                            continue

                        if self.execute_update(msg) == 1:
                            print "max kill exceeded"
                            break
                    self.lock.release()


            except Empty:
                if waiting == 0:
                    logger.debug("Waiting for RS1 mapping...")
                    waiting = 1
                else:
                    waiting = (waiting % 30) + 1
                    if waiting == 30:
                        logger.debug("Waiting for RS1 mapping...")

        logger.debug("closing reception from RS1")


    # process a BGP update message
    def process_update(self):

        logger.info("Starting the Server to handle incoming BGP Updates from ExaBGP. Listening on port " + str(self.member_receiver_port))
        self.server_receive_bgp_messages.start()
        #self.server_send_mpc_output.start()
        #logger.info("RS2 connected to Host Receiver Mock ")

        print str(self.id) + " is ready"
        while True:

            try:
                logger.info("waiting for handler message")
                msg = self.server_receive_bgp_messages.receiver_queue.get(True, 1)
                msg = pickle.loads(msg)
            except Empty:
                continue

            logger.info("received message: " + str(msg))


            if "stop" in msg:
                logger.info("stop received")
                break
            elif "start-rib" in msg:
                logger.info("start-rib received")
                self.rib_state=True
                continue
            elif "finish-rib" in msg:
                time.sleep(1)
                logger.info("finish-rib received")
                self.rib_state=False
                continue

            announcement_id = msg["announcement_id"]
            self.lock.acquire()
            self.id_2_msg[announcement_id] = msg
            execute=False
            if announcement_id in self.id_2_port:
                #send message to the correct worker
                if self.id_2_msg[announcement_id]["prefix"] not in self.prefix_2_bgp_next_hop_2_route.keys():
                    self.prefix_2_bgp_next_hop_2_route[self.id_2_msg[announcement_id]["prefix"]]={}
                logger.info("adding route " + str(announcement_id) + " into the worker queue")
                self.prefix_2_bgp_next_hop_2_route[self.id_2_msg[announcement_id]["prefix"]][self.id_2_msg[announcement_id]["bgp_next_hop"]] = msg
                msg={"announcement_id" : msg["announcement_id"], "messages" : self.prefix_2_bgp_next_hop_2_route[msg["prefix"]]}

                del self.id_2_port[announcement_id]
                del self.id_2_msg[announcement_id]

                if self.rib_state:
                    self.lock.release()
                    continue
                if self.execute_update(msg)==1:
                    print "max kill exceeded"
                    break
            self.lock.release()

        logger.debug("joining RS1 and worker receiver threads ")
        self.receive_mappings_from_rs1_th.join()
        logger.debug("joined RS1 ")

        myinput = "4\n"
        print >> self.p.stdin, myinput # write input
        self.p.stdin.flush() # not necessary in this case

        f=open('statistics-prio-rs2-worker-'+str(self.smpc_port)+'.txt','w')
        f.write("route_id start-processing-time start-smpc-time smpc-time-no-output smpc-time-no-output-input-sent total-time smpc-time\n")
        for bgp_update_id in self.statistics.observations.keys():
            start_processing_time= self.statistics.observations[bgp_update_id]["start-processing-time"]
            start_smpc_time= self.statistics.observations[bgp_update_id]["start-smpc-time"]
            start_smpc_time_input_sent= self.statistics.observations[bgp_update_id]["start-smpc-time-input-sent"]
            smpc_time_no_output_reading= self.statistics.observations[bgp_update_id]["end-smpc-time"] - start_smpc_time
            smpc_time_no_output_reading_input_sent = self.statistics.observations[bgp_update_id]["end-smpc-time"] - start_smpc_time_input_sent
            total_time = self.statistics.observations[bgp_update_id]["end-processing-time"] - self.statistics.observations[bgp_update_id]["start-processing-time"]
            smpc_time = self.statistics.observations[bgp_update_id]["end-processing-time"] - self.statistics.observations[bgp_update_id]["start-smpc-time"]
            f.write(str(bgp_update_id) + " " + str("{0:.9f}".format(start_processing_time)) + " " + str("{0:.9f}".format(start_smpc_time)) + " " + str("{0:.9f}".format(smpc_time_no_output_reading))  + " " + str("{0:.9f}".format(smpc_time_no_output_reading_input_sent)) + " " + str("{0:.9f}".format(total_time)) +" " + str("{0:.9f}".format(smpc_time)) + "\n")
        f.close()


    def execute_update(self,msg):

        list_of_msg_for_prefix=msg["messages"] #remove the route_id used to prioritize the announcements in the queue
        #logger.debug("msg: " + str(msg))

        announcement_id = msg["announcement_id"]
        self.statistics.received_bgp_update(announcement_id)

        # RUNNING THE MPC
        number_of_routes = len(list_of_msg_for_prefix)+1
        logger.info("number of routes: " + str(number_of_routes))

        export_rows_string=""
        logger.info("list_of_msg_for_prefix: " + str(list_of_msg_for_prefix))
        i=0
        for bgp_speaker in self.participant_db.bgp_speaker_2_id.keys():
            sum=0
            for next_hop in list_of_msg_for_prefix.keys():
                sum = sum << 1
                if list_of_msg_for_prefix[next_hop]["encrypted_exp_policies_rs2"][i]==True:
                    sum+=1
            sum = sum << 1
            i+=1
            #export_row_string="{0:#0{1}x}".format(sum,AS_ROW_ENCODING_SIZE/4+2)[2:]
            export_row_string='{num:0{width}x}'.format(num=sum, width=AS_ROW_ENCODING_SIZE/4)
            export_rows_string+=export_row_string
        logger.debug("export_rows_string: " + export_rows_string)

        #add dummy key
        keys_str = DUMMY_KEY

        i=1
        list_of_route_ids = []
        for msg in list_of_msg_for_prefix:
            list_of_route_ids.append(list_of_msg_for_prefix[next_hop]["announcement_id"])
            keys_str+='{num:0{width}x}'.format(num=0, width=KEY_LENGTH*2)
            keys_str+='{num:0{width}x}'.format(num=0, width=KEY_ID_SIZE/4)
            i+=1

        if len(list_of_route_ids)>1:
            local_prefs=""
            #create local prefs file
            #logger.info("member_preference: " + str(self.member_preference.member_2_member_2_local_pref))
            #logger.info("members: " + str(self.participant_db.bgp_speaker_2_id.keys()))
            for bgp_speaker in self.participant_db.bgp_speaker_2_id.keys():
                pre_look_up = self.member_preference.member_2_member_2_local_pref[self.participant_db.bgp_speakers_2_asnumber[bgp_speaker]]
                local_prefs+="00"
                for next_hop in list_of_msg_for_prefix.keys():
                    if next_hop == bgp_speaker:
                        local_pref="00"
                    else:
                        bgp_next_hop  = list_of_msg_for_prefix[next_hop]["bgp_next_hop"]
                        local_pref = pre_look_up[self.participant_db.bgp_speakers_2_asnumber[bgp_next_hop]][RS2]
                    local_prefs+=local_pref
            myinput = "2"+"\n"+str(self.number_of_participants) +"\n" +str(number_of_routes) + "\n" + export_rows_string + "\n" + keys_str +  "\n" + local_prefs + "\n"+ "0"
        else:
            myinput = "1"+"\n"+str(self.number_of_participants) +"\n" +str(number_of_routes) + "\n" + export_rows_string + "\n" + keys_str +  "\n"+ "0"


        # invoking the MPC

        self.statistics.record_start_smpc(announcement_id)
        logger.info("input-to-mpc: " + myinput)
        #self.statistics.record_start_smpc(route_id)
        print >> self.p.stdin, myinput # write input
        self.p.stdin.flush() # not necessary in this case
        self.statistics.record_start_smpc_input_sent(announcement_id)
        logger.debug("reading line")
        #logger.debug(self.p.stdout.readline())

        line = self.reader.readline(1)
        if line == None:
                print "killing ixp.exe on port " + str(self.smpc_port)
                self.p.terminate()
                self.statistics.remove_statistic(announcement_id)
                if self.max_kill >= 0:
                        time.sleep(1) # this should hopefully guarantee that also the other worker is killed
                        args = ("taskset -c " + str(self.smpc_port-FIRST_PORT) + " " +ABY_EXEC_PATH +' -r 1 -o 1 -f 3 -a ' + port_config.process_assignement["rs1"] + ' -p ' + str(self.smpc_port))
                        self.p = subprocess.Popen(args.split(" "),stdout=subprocess.PIPE,stdin=subprocess.PIPE, bufsize=8096)
                        self.reader = NonBlockingStreamReader(self.p.stdout)
                        self.reader.readline(10)
                        #p.stdout.readline()
                        print "re-launching ixp.exe port " + str(self.smpc_port)
                        if_first=True
                        self.max_kill-=1
                        return 0
                else:
                        return 1
        else:
            logger.debug("line: " + line.rstrip())


        self.statistics.record_end_of_smpc_processing(announcement_id)
        str_host=""
        if len(list_of_route_ids)>1:
            line = self.reader.readline(1)
            logger.debug("line: " + line.rstrip())
        for i in range(0,self.number_of_participants):
            #line=self.p.stdout.readline().rstrip()
            line=self.reader.readline(1).rstrip()
            logger.info(line)
            str_host+=line
        #for i in range(0,self.number_of_participants):
        #    self.p.stdout.readline()

        #logger.debug("Sending route to Host. Key: " +str_host)
        #self.statistics.record_end_of_bgp_update_processing(route_id)
        self.statistics.record_end_of_bgp_update_processing(announcement_id)
        ###self.server_send_mpc_output.sender_queue.put(pickle.dumps({"type" : "to-hosts",  "announcement_id" : announcement_id,  "key" : str_host, "list_of_route_ids" : list_of_route_ids}))


def prio_worker2_main(id):
    worker = AllWorker2(int(id))
    worker.process_update()


def main():
    parser = argparse.ArgumentParser()
        # locate config file
    # base_path = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)),"..","examples",args.dir,"config"))
    # config_file = os.path.join(base_path, "sdx_global.cfg")

    parser.add_argument("-i","--id", help="port number", type=int, default=0)
    parser.add_argument("-p","--processes", help="number of processes", type=int, default=1)
    args = parser.parse_args()

    procs = [mp.Process(target=prio_worker2_main, args=(x, )) for x in range(0,int(args.processes))]

    # Run processes
    for p in procs:
        p.start()


    # start route server
    # sdx_rs = route_server(config_file)
    #worker = AllWorker2(int(args.id))
    #worker.process_update()

    for p in procs:
        p.join()

    print "waiting before dying"
    logger.info("waiting before dying")

    for thread in threading.enumerate():
        print thread.name

if __name__ == '__main__':
    main()
