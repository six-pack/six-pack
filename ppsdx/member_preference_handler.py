import util.log
from multiprocessing.connection import Client
import port_config
import pickle
from participant_db import ParticipantDB
import random
import os
import util.crypto_util as crypto

logger = util.log.getLogger('member_preference_handler')

LOCAL_PREF_SIZE=5 # one bit is used to avoid oscillations
CONGESTION_PREF_SIZE=3

class MemberPreferencesHandler:
    def __init__(self):
        self.rs1_client = Client((port_config.process_assignement["localhost"], port_config.port_assignement["rs1-preference-channel"]), authkey=None)
        self.rs2_client = Client((port_config.process_assignement["localhost"], port_config.port_assignement["rs2-preference-channel"]), authkey=None)
        self.member2prefix2pref = {} #TODO: populate and manage this dictionary
        self.member2member2pref = {} #TODO: populate and manage this dictionary
        self.participant_db = ParticipantDB()
        for as_number_1 in self.participant_db.asnumber_2_bgp_speakers.keys():
            for as_number_2 in self.participant_db.asnumber_2_bgp_speakers.keys():
                if as_number_1 == as_number_2:
                    continue
                if as_number_1 not in self.member2member2pref.keys():
                    self.member2member2pref[as_number_1]={}

                #FIXME: generate member's preferences as a hex-string not a boolean array
                self.member2member2pref[as_number_1][as_number_2]=map(lambda x : random.random() > 0.5, [0]*LOCAL_PREF_SIZE)
                self.member2member2pref[as_number_1][as_number_2][LOCAL_PREF_SIZE-1]=0


    def send_new_preference_for_member(self,as_number_sender,as_number_receiver, new_pref, prefix=None):

        new_prefs= self.generate_new_pref(new_pref)

        msg ={"as_number_sender" : as_number_sender, "as_number_receiver" : as_number_receiver, "local_preference" : new_pref[0], "prefix" : prefix}
        self.rs1_client.send(pickle.dumps(msg))

        msg["local_preference"] =new_pref[1]
        self.rs2_client.send(pickle.dumps(msg))


    def generate_new_pref(self, new_pref):
        #generate a random nonce
        new_pref_rs1 = os.urandom(LOCAL_PREF_SIZE)

        new_pref_rs2 = crypto.xor_strings(new_pref_rs1,new_pref.decode("hex"))
        return (new_pref_rs1.encode("hex"),new_pref_rs2.encode("hex"))


    def send_new_preference_for_bgp_next_hop(self,as_number_sender,bgp_next_hop, new_pref, prefix=None):

        new_prefs= self.generate_new_pref(new_pref)

        msg ={"as_number_sender" : as_number_sender, "as_number_receiver" : bgp_next_hop, "local_preference" : new_pref[0], "prefix" : prefix}
        self.rs1_client.send(pickle.dumps(msg))

        msg["local_preference"] =new_pref[1]
        self.rs2_client.send(pickle.dumps(msg))


    def send_new_preference_for_prefix(self,as_number_sender, new_pref, prefix, best_bgp_next_hop):
        for bgp_next_hop in self.participant_db.bgp_speaker_2_id.keys():
            if(bgp_next_hop==best_bgp_next_hop):
                new_pref=self.addOne(new_pref)
            else:
                new_pref=self.addZero(new_pref)
            self.send_new_preference_for_bgp_next_hop(self,as_number_sender,bgp_next_hop, new_pref,prefix)


    def addOne(self,hex_string):
        return '{num:0{width}x}'.format(num=(int(hex_string,16)<<1)+1, width=LOCAL_PREF_SIZE)

    def addZero(self,hex_string):
        return '{num:0{width}x}'.format(num=(int(hex_string,16)<<1), width=LOCAL_PREF_SIZE)