
import pickle
import util.log
from Queue import Empty
import subprocess
from participant_db import ParticipantDB
import port_config
import time
from util.statistics_collector_2 import StatisticsCollector


logger = util.log.getLogger('worker-all-rs2')

ABY_EXEC_PATH="../aby/bin/ixp.exe"
#ABY_EXEC_PATH="/home/vagrant/aby/bin/ixp.exe"

DUMMY_KEY="00000000000000000000000000000000ff"

KEY_LENGTH = 16

AS_ROW_ENCODING_SIZE = 32 # bits

KEY_ID_SIZE = 8 # bits

PARALLELIZATION_FACTOR = 1

MOCK = False

class AllWorker2():

    def __init__(self,port,handler_to_worker_queue,worker_2_handler_queue):
        # get port
        self.port = port

        self.handler_to_worker_queue = handler_to_worker_queue
        self.worker_2_handler_queue = worker_2_handler_queue

        #self.port=7000

        logger.debug("launching ixp.exe on port " + str(self.port))
        if not MOCK:
            time.sleep(1)
            logger.debug(ABY_EXEC_PATH +' -r 1 -o 1 -f 3 -a ' + port_config.process_assignement["rs1"] + ' -p ' + str(self.port))
            args = (ABY_EXEC_PATH +' -r 1 -o 1 -f 3 -a ' + port_config.process_assignement["rs1"] + ' -p ' + str(self.port))
            logger.debug(str(args.split(" ")))
            #args = (ABY_EXEC_PATH +" -r 1 -o 1 -f 1 -a localhost -p " + str(self.port))
            #args = (ABY_EXEC_PATH +' -r 1 -o 1 -f 1  -a localhost')
            self.p = subprocess.Popen(args.split(" "),stdout=subprocess.PIPE,stdin=subprocess.PIPE, bufsize=8096)
            logger.debug("read: " + str(self.p.stdout.readline()))
        logger.debug("process launched")
        self.participant_db = ParticipantDB()
        self.number_of_participants = len(self.participant_db.bgp_speaker_2_id)
        self.mock_counter=0
        self.statistics = StatisticsCollector()


    # process a BGP update message
    def process_update(self):

        while True:

            '''args = (ABY_EXEC_PATH +' -r 1 -o 1 -f 1 -a localhost -p 7765')
            p = subprocess.Popen(args.split(" "),stdout=subprocess.PIPE,stdin=subprocess.PIPE, bufsize=8096)
            p.stdout.readline()

            str="3\n2\n000000000000000000000001\nf2a90c139c4c8093620c4fd35492c6f00000000000000000000000000000000000ff"


            print >> p.stdin, str # write input
            p.stdin.flush() # not necessary in this case
            print p.stdout.readline().rstrip()
            print p.stdout.readline().rstrip()
            print p.stdout.readline().rstrip()
            print p.stdout.readline().rstrip()'''


            try:
                msg = self.handler_to_worker_queue.get(True, 1)
            except Empty:
                continue

            logger.debug("received message: " + str(msg))
            if "stop" in msg:
                break

            route_id = msg["route_id"]
            self.statistics.received_bgp_update(route_id)

            encrypted_export_policy = msg["encrypted_exp_policies_rs2"]

            # RUNNING THE MPC
            # create the export table for the prefix
            number_of_routes = 2
            logger.debug("number of routes: " + str(number_of_routes))
            export_rows_string=""
            for i in range(0,len(self.participant_db.bgp_speaker_2_id.keys())):
                sum =0
                if encrypted_export_policy[i]:
                    sum+=1
                export_row_string="{0:#0{1}x}".format(sum,AS_ROW_ENCODING_SIZE/4+2)[2:]
                export_rows_string+=export_row_string
            #logger.debug("export_rows_string: " + export_rows_string)

            # create the keys input
            key = msg["key"]
            keys_str="{0:#0{1}x}".format(0,2*KEY_LENGTH+KEY_ID_SIZE/4+2)[2:]
            #add dummy key
            keys_str+="0000000000000000000000000000000000"

            if MOCK:
                if self.mock_counter == 0:
                    self.worker_2_handler_queue.put({"type" : "to-hosts", "route_id" : self.mock_counter, "encrypted_route" : "aaaa", "key" : "0" * 34 * 743})
                elif self.mock_counter == 1:
                    self.worker_2_handler_queue.put({"type" : "to-hosts", "route_id" : self.mock_counter, "encrypted_route" : "bbbb", "key" : "0" * 34 * 743})
                elif self.mock_counter == 2:
                    self.worker_2_handler_queue.put({"type" : "to-hosts", "route_id" : self.mock_counter, "encrypted_route" : "cccc", "key" : "0" * 34 * 743})
                else :
                    self.worker_2_handler_queue.put({"type" : "to-hosts", "route_id" : self.mock_counter, "encrypted_route" : "dddd", "key" : "0" * 34 * 743})
                self.mock_counter+=1
            else:
                # invoking the MPC
                myinput = "1"+"\n"+str(self.number_of_participants) +"\n" +str(number_of_routes) + "\n" + export_rows_string + "\n" + keys_str + "\n"+ "0"
                '''ff=open('smpc-input.txt','w')
                ff.write(myinput)
                ff.flush()
                ff.close()'''
                logger.debug("input-to-mpc: " + myinput)
                self.statistics.record_start_smpc(route_id)
                print >> self.p.stdin, myinput # write input
                self.p.stdin.flush() # not necessary in this case
                self.statistics.record_start_smpc_input_sent(route_id)
                self.p.stdout.readline()
                self.statistics.record_end_of_smpc_processing(route_id)
                str_host=""
                for i in range(0,self.number_of_participants):
                    line=self.p.stdout.readline().rstrip()
                    logger.debug(line)
                    str_host+=line
                    #if i==0:
                    #    self.statistics.record_end_of_smpc_processing(route_id)
                logger.debug("smpc output computed")
                self.statistics.record_end_of_bgp_update_processing(route_id)
                self.worker_2_handler_queue.put({"type" : "to-hosts",  "route_id" : route_id, "key" : str_host})

        if not MOCK:
            myinput = "255\n"
            print >> self.p.stdin, myinput # write input
            self.p.stdin.flush() # not necessary in this case

        self.worker_2_handler_queue.put({"stop" : None})

        f=open('statistics-all-rs2-worker-'+str(self.port)+'.txt','w')
        f.write("route_id start-processing-time start-smpc-time smpc-time-no-output smpc-time-no-output-input-sent total-time smpc-time\n")
        for bgp_update_id in self.statistics.observations.keys():
            start_processing_time= self.statistics.observations[bgp_update_id]["start-processing-time"]
            start_smpc_time= self.statistics.observations[bgp_update_id]["start-smpc-time"]
            start_smpc_time_input_sent= self.statistics.observations[bgp_update_id]["start-smpc-time-input-sent"]
            smpc_time_no_output_reading= self.statistics.observations[bgp_update_id]["end-smpc-time"] - start_smpc_time
            smpc_time_no_output_reading_input_sent = self.statistics.observations[bgp_update_id]["end-smpc-time"] - start_smpc_time_input_sent
            total_time = self.statistics.observations[bgp_update_id]["end-processing-time"] - self.statistics.observations[bgp_update_id]["start-processing-time"]
            smpc_time = self.statistics.observations[bgp_update_id]["end-processing-time"] - self.statistics.observations[bgp_update_id]["start-smpc-time"]
            f.write(str(bgp_update_id) + " " + str("{0:.15f}".format(start_processing_time)) + " " + str("{0:.15f}".format(start_smpc_time)) + " " + str("{0:.15f}".format(smpc_time_no_output_reading))  + " " + str("{0:.15f}".format(smpc_time_no_output_reading_input_sent)) + " " +str(total_time) +" " + str(smpc_time) + "\n")
        f.close()



def all_worker_main(port,handler_to_worker_queue,worker_2_handler_queue):
    worker = AllWorker2(port,handler_to_worker_queue,worker_2_handler_queue)
    worker.process_update()
