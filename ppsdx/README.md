# xbgp - PP-SDX


```bash
> pip install pycrypto
```


## Start logging server

```bash
$ python iSDX/logServer.py test-ms
```

## Start PPSDX route server

```bash
$ python iSDX/ppsdx/pprs.py
```

## Run xbgp

```bash
$ python xbgp.py -d --speedup 1 localhost 6000 xrs updates.20150802.0000.pruned.txt 5 0
```
