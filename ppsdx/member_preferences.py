from participant_db import ParticipantDB
import os
from util.singleton import Singleton



class MemberPreferences(Singleton):

	def __init__(self):
		self.member_2_member_2_local_pref = {  }

		self.participant_db = ParticipantDB()

		for as_number_1 in self.participant_db.asnumber_2_bgp_speakers.keys():
			self.member_2_member_2_local_pref[as_number_1]={}
			for as_number_2 in self.participant_db.asnumber_2_bgp_speakers.keys():
				if as_number_1 == as_number_2:
					#self.member_2_member_2_local_pref[as_number_1][as_number_2]={"rs1" : "00", "rs2" :"00"}
					self.member_2_member_2_local_pref[as_number_1][as_number_2]=[ "00", "00"]
				else:
					#self.member_2_member_2_local_pref[as_number_1][as_number_2]={"rs1" : os.urandom(1).encode("hex"), "rs2" :os.urandom(1).encode("hex")}
					self.member_2_member_2_local_pref[as_number_1][as_number_2]=[os.urandom(1).encode("hex"), os.urandom(1).encode("hex")]






