
import pickle
import util.log
from Queue import Empty
import subprocess
from participant_db import ParticipantDB
from util.statistics_collector_2 import StatisticsCollector
import port_config
import time
from util.nbstreamreader import NonBlockingStreamReader

logger = util.log.getLogger('worker-all-rs1')

ABY_EXEC_PATH="../aby/bin/ixp.exe"
#ABY_EXEC_PATH="/home/vagrant/aby/bin/ixp.exe"

DUMMY_KEY="00000000000000000000000000000000ff"

KEY_LENGTH = 16

AS_ROW_ENCODING_SIZE = 32 # bits

KEY_ID_SIZE = 8 # bits

PARALLELIZATION_FACTOR = 1

MOCK = False


class AllWorker1():

    def __init__(self,handler_to_worker_queue,worker_to_handler_queue, workers_pool):
        # get port
        self.port = workers_pool.get()
        worker_to_handler_queue.put({"type" : "to-rs2-init", "worker_port" : self.port})
        self.worker_to_handler_queue = worker_to_handler_queue
        self.workers_pool = workers_pool
        self.handler_to_worker_queue = handler_to_worker_queue

        #self.port=7000
        logger.debug("launching ixp.exe on port " + str(self.port))
        if not MOCK:
            logger.debug(ABY_EXEC_PATH +' -r 0 -o 1 -f 3 -a 0.0.0.0 -p ' + str(self.port))
            args = (ABY_EXEC_PATH +' -r 0 -o 1 -f 3 -a 0.0.0.0 -p ' + str(self.port))
            logger.debug(str(args.split(" ")))
            #args = (ABY_EXEC_PATH +' -r 0 -o 1 -f 1 -a localhost -p ' + str(self.port))
            self.p = subprocess.Popen(args.split(" "),stdout=subprocess.PIPE,stdin=subprocess.PIPE, bufsize=8096)
            #self.reader = NonBlockingStreamReader(self.p.stdout)
            #logger.debug("read: " + str(self.reader.readline(10000)))
            logger.debug("read: " + str(self.p.stdout.readline()))

        logger.debug("process launched")
        self.participant_db = ParticipantDB()
        self.number_of_participants = len(self.participant_db.bgp_speaker_2_id)
        self.mock_counter=0
        self.statistics = StatisticsCollector()



# process a BGP route
    def process_update(self):
        waiting =0
        print str(self.port) + " is ready"
        logger.debug("waiting for handler message")
        isFirst=True
        while True:

            try:

                msg = self.handler_to_worker_queue.get(True, 1)

            except Empty:
                if waiting == 0:
                    logger.debug("Waiting for handler message...")
                    waiting = 1
                else:
                    waiting = (waiting % 30) + 1
                    if waiting == 30:
                        logger.debug("Waiting for handler message...")
                continue

            if "stop" in msg:
                logger.debug("stop received")
                break

            #logger.debug("msg: " + str(msg))
            route_id = msg["route_id"]
            self.statistics.received_bgp_update(route_id)
            self.worker_to_handler_queue.put({"type" : "to-rs2", "route_id" : route_id, "worker_port" : self.port})

            encrypted_route = msg["encrypted_route"]
            encrypted_export_policy = msg["encrypted_exp_policies_rs1"]

            # RUNNING THE MPC
            # create the export table for the prefix
            number_of_routes = 2

            export_rows_string=""
            for i in range(0,len(self.participant_db.bgp_speaker_2_id.keys())):
                sum =0
                if encrypted_export_policy[i]:
                    sum+=1
                export_row_string="{0:#0{1}x}".format(sum,AS_ROW_ENCODING_SIZE/4+2)[2:]
                export_rows_string+=export_row_string
            #logger.debug("export_rows_string: " + export_rows_string)

            # create the keys input
            key = msg["key"]
            keys_str=key
            keys_str+="{0:#0{1}x}".format(0,KEY_ID_SIZE/4+2)[2:]
            #add dummy key
            keys_str+=DUMMY_KEY

            # invoking the MPC
            if MOCK:
                if self.mock_counter == 0:
                    self.worker_to_handler_queue.put({"type" : "to-hosts", "route_id" : self.mock_counter, "encrypted_route" : "aaaa", "key" : "0" * 34 * 743})
                elif self.mock_counter == 1:
                    self.worker_to_handler_queue.put({"type" : "to-hosts", "route_id" : self.mock_counter, "encrypted_route" : "bbbb", "key" : "0" * 34 * 743})
                elif self.mock_counter == 2:
                    self.worker_to_handler_queue.put({"type" : "to-hosts", "route_id" : self.mock_counter, "encrypted_route" : "cccc", "key" : "0" * 34 * 743})
                else :
                    self.worker_to_handler_queue.put({"type" : "to-hosts", "route_id" : self.mock_counter, "encrypted_route" : "dddd", "key" : "0" * 34 * 743})
                self.mock_counter+=1
            else:
                #print route_id
                myinput = "1"+"\n"+str(self.number_of_participants) +"\n" +str(number_of_routes) + "\n" + export_rows_string + "\n" + keys_str + "\n"+ "0"
                '''ff=open('smpc-input.txt','w')
                ff.write(myinput)
                ff.flush()
                ff.close()'''
                logger.debug("input-to-mpc: " + myinput)
                self.statistics.record_start_smpc(route_id)
                print >> self.p.stdin, myinput # write input
                self.p.stdin.flush() # not necessary in this case
                self.statistics.record_start_smpc_input_sent(route_id)
                self.p.stdout.readline()
                timer=1
                if isFirst:
                    timer=1
                    isFirst=False
                if 1 == 0: 
                #if self.reader.readline(1) == None:
                    #kill the worker!
                    logger.debug("killing the worker")
                    self.p.terminate()
                    self.statistics.remove_statistic(route_id) #TODO
                    self.statistics.increase_killed_workers()
                    time.sleep(1) # this should hopefully guarantee that also the other worker is killed
                    logger.debug(ABY_EXEC_PATH +' -r 0 -o 1 -f 3 -a 0.0.0.0 -p ' + str(self.port))
                    args = (ABY_EXEC_PATH +' -r 0 -o 1 -f 3 -a 0.0.0.0 -p ' + str(self.port))
                    logger.debug(str(args.split(" ")))
                    #args = (ABY_EXEC_PATH +' -r 0 -o 1 -f 1 -a localhost -p ' + str(self.port))
                    self.p = subprocess.Popen(args.split(" "),stdout=subprocess.PIPE,stdin=subprocess.PIPE, bufsize=8096)
                    self.reader = NonBlockingStreamReader(self.p.stdout)
                    logger.debug("read: " + str(self.reader.readline(10000)))
                    #logger.debug("read: " + str(self.p.stdout.readline()))

                else:
                    self.statistics.record_end_of_smpc_processing(route_id)
                    logger.debug("empty line read")
                    str_host=""
                    for i in range(0,self.number_of_participants):
                        line=self.p.stdout.readline().rstrip()
                        #line=self.reader.readline(2).rstrip()
                        logger.debug(line)
                        str_host+=line
                    #logger.debug("Sending route to Host. Key: " +str_host)
                    self.statistics.record_end_of_bgp_update_processing(route_id)
                    self.worker_to_handler_queue.put({"type" : "to-hosts",  "route_id" : route_id, "encrypted_route" : encrypted_route, "key" : str_host})
            #self.server_send_mpc_output.sender_queue.put(pickle.dumps({"encrypted_route" : encrypted_route, "key" : str_host}))


        #close the process
        if not MOCK:
            myinput = "255\n"
            print >> self.p.stdin, myinput # write input
            self.p.stdin.flush() # not necessary in this case

        self.worker_to_handler_queue.put({"stop" : None , "port" : self.port})

        f=open('statistics-all-rs1-worker-'+str(self.port)+'.txt','w')
        f.write("route_id start-processing-time start-smpc-time smpc-time-no-output smpc-time-no-output-input-sent total-time smpc-time\n")
        for bgp_update_id in self.statistics.observations.keys():
            start_processing_time= self.statistics.observations[bgp_update_id]["start-processing-time"]
            start_smpc_time= self.statistics.observations[bgp_update_id]["start-smpc-time"]
            start_smpc_time_input_sent= self.statistics.observations[bgp_update_id]["start-smpc-time-input-sent"]
            smpc_time_no_output_reading= self.statistics.observations[bgp_update_id]["end-smpc-time"] - start_smpc_time
            smpc_time_no_output_reading_input_sent = self.statistics.observations[bgp_update_id]["end-smpc-time"] - start_smpc_time_input_sent
            total_time = self.statistics.observations[bgp_update_id]["end-processing-time"] - self.statistics.observations[bgp_update_id]["start-processing-time"]
            smpc_time = self.statistics.observations[bgp_update_id]["end-processing-time"] - self.statistics.observations[bgp_update_id]["start-smpc-time"]
            f.write(str(bgp_update_id) + " " + str("{0:.9f}".format(start_processing_time)) + " " + str("{0:.9f}".format(start_smpc_time)) + " " + str("{0:.9f}".format(smpc_time_no_output_reading))  + " " + str("{0:.9f}".format(smpc_time_no_output_reading_input_sent)) + " " + str("{0:.9f}".format(total_time))  +" " + str("{0:.9f}".format(smpc_time)) + "\n")
        f.close()

def all_worker_main(handler_to_worker_queue,workers_to_rs2_queue, workers_pool):
    worker = AllWorker1(handler_to_worker_queue,workers_to_rs2_queue, workers_pool)
    worker.process_update()
