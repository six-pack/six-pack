# SIXPACK: Securing Internet eXchange Points Against Curious onlooKers

## Installation: Vagrant Setup

###Prerequisite

To get started install these softwares on your ```host``` machine:

1. Install ***Vagrant***, it is a wrapper around virtualization softwares like VirtualBox, VMWare etc.: http://www.vagrantup.com/downloads

2. Install ***VirtualBox***, this would be your VM provider: https://www.virtualbox.org/wiki/Downloads

3. Install ***Git***, it is a distributed version control system: https://git-scm.com/downloads

###Setup
1. Clone our git repository: 
```$git clone git@bitbucket.org:six-pack/six-pack.git sixpack```
2. ```$cd sixpack```
3. ```$vagrant up```
4. wait until the machine is provisioned, then execute
5. ```$vagrant ssh```

### Running the ``throughput test'' evaluation:
1. Attach to the tmux session that is automatically setup
```$tmux attach -t sixpack```
2. Press Ctrl+x and 1 to switch to window 1. Execute
```$python logServer.py ms``` 
3. Press Ctrl+x and 2 to switch to window 2. Execute
```$python prio_worker_rs1_hashed.py -p 1```
This command will start the first SMPC-enabled instance of the SIXPACK route server. The '-p' parameter represents the number of parallel workers. In our paper we tested with 'p={1,2,4,8}'.
4. Press Ctrl+x and 3 to switch to window 3. Execute
```$python prio_worker_rs2_hashed.py -p 1```
This command will start the second SMPC-enabled instance of the SIXPACK route server.
5. Press Ctrl+x and 4 to switch to window 4. Execute
```$./send-bgp-announcements-prio-hashed.sh 1 4```
This command will start the generation of BGP messages as in the ``Throughput test'' from our paper.
6. press Ctrl+x and 1 to switch to window 1. Check the logger output. To scroll the window in tmux, press Ctrl+x and [. Ctrl+x and d to detach. To obtain perfromaning results, the log should be disabled by setting its state to CRITICAL in 'util.log.py'. When the test ends, statistics can be found in the 'ppsdx' folder where the first route server instance is executed. A './compute-throughput [number-of-parallel-workers]' can be used to compute the achieved throughput in terms of number of BGP messages processed per second.
