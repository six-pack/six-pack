#!/usr/bin/env bash

cd ~

# Install PyCrypto
sudo pip install pycrypto
sudo pip install netaddr

# create a mock participant DB
cd sixpack/data
./participant_db.sh ip-2-as-simple-600.txt
cd ..
cd xbgp/throughput-test
./create-update-file-all.sh
cd ..
cd ..
