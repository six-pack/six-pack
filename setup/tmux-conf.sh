#!/bin/sh

cp ~/sixpack/.tmux.conf ~/

sn=sixpack
cd sixpack

cp .tmux.conf ../

tmux new-session -s $sn -d
tmux new-window -t "$sn:1" -n "logger"
tmux send-keys -t "$sn:1" 'python logServer.py'
cd ppsdx
tmux new-window -t "$sn:2" -n "rs1"
tmux send-keys -t "$sn:2" ' python prio_worker_rs1_hashed.py -p 1'
tmux new-window -t "$sn:3" -n "rs2"
tmux send-keys -t "$sn:3" 'python prio_worker_rs2_hashed.py -p 1'
#tmux new-window -t "$sn:4" -n "member receiver mock"
#tmux send-keys -t "$sn:4" './receive-routes-all.sh > /dev/null'
cd ../xbgp/
tmux new-window -t "$sn:4" -n "member sender mock"
tmux send-keys -t "$sn:4" './send-bgp-announcements-prio-hashed.sh 1 4'
cd ..

tmux select-window -t "$sn:1"
#tmux -2 attach-session -t "$sn"
