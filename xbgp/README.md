# xbgp - ExaBGP Simulator

This module reads bgp updates from a bgp dump file and sends them in ExaBGP format to xrs

## Run xbgp

```bash
$ ./xbgp.py localhost 6000 xrs updates.20150802.0000.txt -d --speedup 1
```
