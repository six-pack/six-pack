#!/bin/bash

cat update-head.txt > update-input.txt
for x in `seq 1 $2`
do
    cat update-base.txt >> update-input.txt
done
python xbgp-prio-hashed.py -d --speedup 1 localhost 6000 xrs update-input.txt 5 0 --processes $2
