
import util.log
from Queue import Empty
import pickle
from util.statistics_collector_2 import StatisticsCollector
import time
from multiprocessing.connection import Client
import ppsdx.port_config as port_config


logger = util.log.getLogger('xbgp-process-hashed')


class XBGPProcess():

    def __init__(self,id, handler_to_worker_queue,throughput):
        # get port

        self.handler_to_worker_queue = handler_to_worker_queue
        self.statistics = StatisticsCollector()
        self.id = id
        self.throughput = throughput

        self.conn1 = Client((port_config.process_assignement["rs1"], port_config.ports_assignment["rs1_receive_bgp_messages"]+self.id), authkey=None)
        self.conn2 = Client((port_config.process_assignement["rs2"], port_config.ports_assignment["rs2_receive_bgp_messages"]+self.id), authkey=None)

        self.counter_announcement=0
        self.counter_prefix=0



   # process a BGP route
    def process_update(self):

        print str(self.id) + " is ready"
        while True:

            try:
                logger.info("waiting for handler message")
                route = self.handler_to_worker_queue.get(True, 1)
            except Empty:
                continue

            if "stop" in route:
                time.sleep(1)
                print "sending STOP " + str(self.id)
                self.conn1.send(pickle.dumps(route))
                self.conn2.send(pickle.dumps(route))
                break

            if self.throughput:
                if "start-rib" in route:
                    print "sending start test message "
                    self.conn1.send(pickle.dumps(route))
                    self.conn2.send(pickle.dumps(route))
                elif "finish-rib" in route:
                    print "sending finish test message "
                    self.conn1.send(pickle.dumps(route))
                    self.conn2.send(pickle.dumps(route))
                elif self.throughput:
                    x=100 # NUMBER_OF_ROUTES
                    self.counter_prefix=0
                    for x in range(0,x):
                        route["announcement_id"]=self.counter_announcement+(self.id*10000)
                        route["prefix"]=self.counter_prefix
                        self.counter_prefix+=1
                        self.counter_announcement+=1
                        self.statistics.xbgp_update_send_update(route["announcement_id"])
                        logger.info("sending route " + str(route["announcement_id"])  + " id:"  + str(self.id)  )
                        self.send_update_rs1(route)
                        self.send_update_rs2(route)
            else:
                self.statistics.xbgp_update_send_update(route["announcement_id"])
                logger.info("sending route " + str(route["announcement_id"])  + " id:"  + str(self.id)  )
                self.send_update_rs1(route)
                self.send_update_rs2(route)

        f=open('statistics-xbgp-prio-'+str(self.id)+'.txt','w')
        f.write("announcement_id start_xbgp_processing_time end_xbgp_processing_time xbgp_update_send_update\n")
        for bgp_update_id in self.statistics.observations.keys():
            start_xbgp_processing_time = self.statistics.observations[bgp_update_id]["start-xbgp-processing-time"]
            end_xbgp_processing_time = self.statistics.observations[bgp_update_id]["end-xbgp-processing-time"]
            xbgp_update_send_update = self.statistics.observations[bgp_update_id]["xbgp_update_send_update"]

            f.write(str(bgp_update_id) + " " + str("{0:.15f}".format(start_xbgp_processing_time)) +" " + str("{0:.15f}".format(end_xbgp_processing_time))+" " + str("{0:.15f}".format(xbgp_update_send_update)) + "\n")


    def send_update_rs1(self, update):
        self.conn1.send(pickle.dumps(update))

    def send_update_rs2(self, update):
        self.conn2.send(pickle.dumps(update))

def send_main(id, handler_to_worker_queue, throughput):
    process = XBGPProcess(id, handler_to_worker_queue,throughput)
    process.process_update()
