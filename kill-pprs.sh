#!/bin/bash
kill -9 `ps aux | grep xbgp | awk '{str=str " " $2 } END{print str}'`
kill -9 `ps aux | grep hashed | awk '{str=str " " $2 } END{print str}'`
kill -9 `ps aux | grep mock | awk '{str=str " " $2 } END{print str}'`
kill -9 `ps aux | grep ixp.exe | awk '{str=str " " $2 } END{print str}'`
