import sys
import subprocess
import time

def main(iterations, mode, number_of_participants,number_of_routes,port,core):
	#ABY_EXEC_PATH="taskset -c " + core + "," + str(int(core)+16)+ " ../ixp.exe"
	'''ABY_EXEC_PATH="taskset -c 0,1,2 ../ixp.exe"
	if core == "1":
                ABY_EXEC_PATH="taskset -c 3,4,5 ../ixp.exe"
        if core == "2":
                ABY_EXEC_PATH="taskset -c 6,7,8 ../ixp.exe"
        if core == "3":
                ABY_EXEC_PATH="taskset -c 9,10,11 ../ixp.exe"
        if core == "4":
                ABY_EXEC_PATH="taskset -c 12,13,14 ../ixp.exe"'''
        ABY_EXEC_PATH="taskset -c 0,1,16,17 ../ixp.exe"
        if core == "1":
                ABY_EXEC_PATH="taskset -c 2,3,18,19 ../ixp.exe"
        if core == "2":
                ABY_EXEC_PATH="taskset -c 4,5,20,21 ../ixp.exe"
        if core == "3":
                ABY_EXEC_PATH="taskset -c 6,7,22,23 ../ixp.exe"
        if core == "4":
                ABY_EXEC_PATH="taskset -c 8,9,24,25 ../ixp.exe"
        if core == "5":
                ABY_EXEC_PATH="taskset -c 10,11,26,27 ../ixp.exe"
        if core == "6":
                ABY_EXEC_PATH="taskset -c 12,13,28,29 ../ixp.exe"
        if core == "7":
                ABY_EXEC_PATH="taskset -c 14,15,30,31 ../ixp.exe"
	#ABY_EXEC_PATH="../ixp.exe"
	export_rows_string="00000002"*number_of_participants
	keys_str="47620fb9f99440fb5316d7eac8d8b9fe01"*number_of_routes
	local_prefs="0"*number_of_routes * number_of_participants * 2
	myinput = str(number_of_participants) +"\n" +str(number_of_routes) + "\n" + export_rows_string + "\n" + keys_str + "\n" + local_prefs 

	args = (ABY_EXEC_PATH +' -r 1 -a 192.168.102.2 -o 1 -f 2 -p ' + port)
	print args.split(" ")
	#args = (ABY_EXEC_PATH +' -r 0 -a localhost -o 1 -f 1 -p ' + port)
	p = subprocess.Popen(args.split(" "),stdout=subprocess.PIPE,stdin=subprocess.PIPE, bufsize=8096)
        p.stdout.readline()
	#print "empty line read"
	is_first=True

	smpc_time=0
	max_kill=100
	initial_time=time.time()
	for x in range(0,iterations):
		#measure from here - discard the first 100 samples
		print >> p.stdin, myinput 
		p.stdin.flush()
		#print "input sent"
		#print myinput
		'''wait=1
                if is_first:
			wait=10'''
                line=p.stdout.readline()
		print line
                '''if line == None:
			print "ready to kill on port " + port
                        p.terminate()
			print "killed"
			if max_kill >= 0:
				print "sleeping"
                        	time.sleep(1) # this should hopefully guarantee that also the other worker is killed
				print "awaking"
				args = (ABY_EXEC_PATH +' -r 1 -a 192.168.102.2 -o 1 -f 3 -p ' + port)
                        	p = subprocess.Popen(args.split(" "),stdout=subprocess.PIPE,stdin=subprocess.PIPE, bufsize=8096)
				p.stdout.readline()
				
				if_first=True
				max_kill-=1
				continue
			else:
				break
                is_first=False'''
		#print "line read: " + line
		#print (time.time() - initial_time) # measure to here - SMPC time execution
	       	str_host=""
		p.stdout.readline().rstrip()
	       	for i in range(0,number_of_participants):
	        	line=p.stdout.readline().rstrip()
			#print str(i) + " " + line
	           	str_host+=line
		#line=p.stdout.readline().rstrip()
		#print line
	print (time.time() - initial_time)
	print >> p.stdin, "0"
	p.stdin.flush()


if __name__ == "__main__":
    main(int(sys.argv[1]),sys.argv[2],int(sys.argv[3]),int(sys.argv[4]),sys.argv[5],sys.argv[6])
