import sys
import subprocess
import time


def main(iterations, mode, number_of_participants,number_of_routes,port,core):
	ABY_EXEC_PATH="taskset -c " + core + " ../ixp.exe"
	export_rows_string="00000002"*number_of_participants
	keys_str="47620fb9f99440fb5316d7eac8d8b9fe01"*number_of_routes
	myinput = str(mode) + "\n" + str(number_of_participants) +"\n" +str(number_of_routes) + "\n" + export_rows_string + "\n" + keys_str + "\n" + "0"

	args = (ABY_EXEC_PATH +' -r 0 -a 0.0.0.0 -o 1 -f 3 -p ' + port)
	print args.split(" ")
	#args = (ABY_EXEC_PATH +' -r 0 -a localhost -o 1 -f 1 -p ' + port)
	p = subprocess.Popen(args.split(" "),stdout=subprocess.PIPE,stdin=subprocess.PIPE, bufsize=8096)
	p.stdout.readline()
	is_first=True
	smpc_time=0

	initial_time=time.time()
	max_kill=100
	for x in range(0,iterations):
		#measure from here - discard the first 100 samples
		#print "sending input"
		print >> p.stdin, myinput
		p.stdin.flush()
		#print "input sent"
		#print myinput
		wait=1
                if is_first:
                        wait=10
	        line=p.stdout.readline()
                if line == None:
			print "killing ixp.exei on port " + port
			p.terminate()
			if max_kill >= 0:
				time.sleep(1) # this should hopefully guarantee that also the other worker is killed
				args = (ABY_EXEC_PATH +' -r 0 -a 0.0.0.0 -o 1 -f 3 -p ' + port)
                		p = subprocess.Popen(args.split(" "),stdout=subprocess.PIPE,stdin=subprocess.PIPE, bufsize=8096)
				p.stdout.readline()
				print "re-launching ixp.exe port " + port
				if_first=True
                                max_kill-=1
				continue
			else:
                                break
                is_first=False
		#print "line read"
		#print (time.time() - initial_time) # measure to here - SMPC time execution
	       	str_host=""
	       	for i in range(0,number_of_participants):
	        	line=p.stdout.readline().rstrip()
			#print str(i) + " " + line
	           	str_host+=line
		#line=p.stdout.readline().rstrip()
		#print line
	print core + " " + str(time.time() - initial_time) # measure to here - SMPC time execution
	print >> p.stdin, "4"
	p.stdin.flush()


if __name__ == "__main__":
    main(int(sys.argv[1]),sys.argv[2],int(sys.argv[3]),int(sys.argv[4]),sys.argv[5],sys.argv[6])
