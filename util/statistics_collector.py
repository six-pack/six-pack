
import time

class StatisticsCollector:
    def __init__(self):
        self.observations={}

    def received_bgp_update(self,bgp_update_id):
        self.bgp_update_id = bgp_update_id
        #self.observations[bgp_update_id] = {"routes" : {}}
        self.bgp_update_initial_time = time.time()

    def record_end_of_bgp_update_processing(self,bgp_update_id):
        self.observations[bgp_update_id]["total-time"] = time.time() - self.bgp_update_initial_time

    def processing_route(self,route_id):
        self.observations[self.bgp_update_id]["routes"][route_id]= {}
        self.route_initial_time = time.time()

    def record_time_before_creating_input_for_invoking_mpc(self,route_id):
        self.observations[self.bgp_update_id]["routes"][route_id]["total-time-python-no-mpc"] = time.time() - self.route_initial_time

    def record_time_before_invoking_mpc(self,route_id):
        self.route_time_before_invoking_mpc=time.time()
        self.observations[self.bgp_update_id]["routes"][route_id]["total-time-before-mpc"] = self.route_time_before_invoking_mpc - self.route_initial_time

    def record_time_after_mpc_execution(self,route_id):
        final_time = time.time()
        self.observations[self.bgp_update_id]["routes"][route_id]["total-time-after-mpc"] = final_time - self.route_initial_time
        self.observations[self.bgp_update_id]["routes"][route_id]["total-mpc-time"] = final_time -  self.route_time_before_invoking_mpc

    def __repr__(self):
        return self.observations.__repr__()

    def __str__(self):
        return self.observations.__str__()