
import time

class StatisticsCollector:
    def __init__(self):
        self.observations={}
        self.killed_workers=0
        self.temp=-1

    def xbgp_update_processing(self,bgp_update_id):
        self.observations[bgp_update_id]={}
        self.observations[bgp_update_id]["start-xbgp-processing-time"] = time.time()

    def xbgp_update_end_processing(self,bgp_update_id):
        self.observations[bgp_update_id]["end-xbgp-processing-time"] = time.time()

    def xbgp_update_send_update(self,bgp_update_id):
        if bgp_update_id not in self.observations:
            self.observations[bgp_update_id]={}
            self.observations[bgp_update_id]["start-xbgp-processing-time"] = -1
            self.observations[bgp_update_id]["end-xbgp-processing-time"] = -1
        self.observations[bgp_update_id]["xbgp_update_send_update"] = time.time()

    def member_start_decryption(self,bgp_update_id,i):
        if bgp_update_id not in self.observations:
            self.observations[bgp_update_id]={}
        self.observations[bgp_update_id][i]={"member_start_decryption" : time.time()}

    def member_end_decryption(self,bgp_update_id,i):
        self.observations[bgp_update_id][i]["member_end_decryption"] = time.time()

    def received_bgp_update(self,bgp_update_id):
        if bgp_update_id not in self.observations:
            self.observations[bgp_update_id] = {}
        self.observations[bgp_update_id]["start-processing-time"] = time.time()
        if self.temp != -1:
            self.observations[bgp_update_id]["wait-receive-time"] = self.temp

    def lock_acquired(self,bgp_update_id):
        if bgp_update_id not in self.observations:
            self.observations[bgp_update_id] = {}
        self.observations[bgp_update_id]["lock-waiting-time"] = time.time()

    def route_inserted(self,bgp_update_id):
        if bgp_update_id not in self.observations:
            self.observations[bgp_update_id] = {}
        self.observations[bgp_update_id]["route-inserted-time"] = time.time()

    def wait_to_receive(self):
        self.temp = time.time()



    def record_start_smpc(self,bgp_update_id):
        self.observations[bgp_update_id]["start-smpc-time"] = time.time()

    def record_start_smpc_input_sent(self,bgp_update_id):
        self.observations[bgp_update_id]["start-smpc-time-input-sent"] = time.time()

    def pre_export(self,bgp_update_id):
        self.observations[bgp_update_id]["pre_export"] = time.time()

    def pre_keys(self,bgp_update_id):
        self.observations[bgp_update_id]["pre_keys"] = time.time()

    def pre_prefs(self,bgp_update_id):
        self.observations[bgp_update_id]["pre_prefs"] = time.time()

    def record_end_of_bgp_update_processing(self,bgp_update_id):
        self.observations[bgp_update_id]["end-processing-time"] = time.time()

    def record_end_of_smpc_processing(self,bgp_update_id):
        self.observations[bgp_update_id]["end-smpc-time"] = time.time()

    def route_inserted_into_queue(self, bgp_update_id):
        if bgp_update_id not in self.observations:
            self.observations[bgp_update_id] = {}
        self.observations[bgp_update_id]["route-to-worker-time"] = time.time()

    def increase_killed_workers(self):
        self.killed_workers+=1

    def remove_statistic(self,route_id):
        del self.observations[route_id]

    def __repr__(self):
        return self.observations.__repr__()

    def __str__(self):
        return self.observations.__str__()