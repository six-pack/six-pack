#!/bin/bash

# Creates the file participant_db.py, which is necessary to run the prototype.
# It requires in input a file with "ip","as" rows


echo "class ParticipantDB:" > participant_db.py
echo "def __init__(self):" | awk '{print "\t" $0}' >> participant_db.py
cat $1 | awk -v FS="," 'BEGIN{str="\t\tself.bgp_speaker_2_id = { ";i=0} {if(NR>1){if(l==1){str=str ", "}l=1;str=str " \"" $1 "\" : " i ;i+=1;}} END{print str " }"}' >> participant_db.py
cat $1 | sed "s/,/ /" | sort -n -k 2 | \
	awk '{if(NR>1){if($2!=last){if(l==1){print str  " ]";} str ="\"" $2 "\" : [ \""  $1 "\"" }else{str=str ",\"" $1 "\""};last=$2;l=1}} END{print str}'  	| awk 'BEGIN{str="\t\tself.asnumber_2_bgp_speakers = {"} {if(l==1){str = str ", "}else{l=1}str = str   $0 } END{print str " ] }"}' >> participant_db.py
cat $1 | awk -v FS="," 'BEGIN{str="\t\tself.id_2_bgp_speaker = { ";i=0} {if(NR>1){if(l==1){str=str ", "}l=1;str=str " "  i " : \"" $1 "\"" ;i+=1;}} END{print str " }"}' >> participant_db.py
cat $1 | sed "s/,/ /" | sort -n -k 1 | \
	awk '{if(NR>1){if($1!=last){if(l==1){print str  " ]";} str ="\"" $1 "\" : [ \""  $2 "\"" }else{str=str ",\"" $2 "\""};last=$1;l=1}} END{print str}'  	| awk 'BEGIN{str="\t\tself.bgp_speakers_2_asnumber = {"} {if(l==1){str = str ", "}else{l=1}str = str   $0 } END{print str " ] }"}' | sed "s/\[//g" | sed "s/\]//g">> participant_db.py

mv participant_db.py ../ppsdx
 
